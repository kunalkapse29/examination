<?php
include 'connection.php';
session_start();
$response = [];
$date = date("Y-m-d");
$sql = "SELECT id,examtitle,examdesc,DATE_FORMAT(startdate,'%W,%e %M %Y') AS startdate, DATE_FORMAT(enddate,'%W,%e %M %Y') AS enddate,hour,minute,second,totalquestion,sectionname,totalmarks FROM createexam WHERE startdate <='$date' and enddate >='$date' and flag=1";
$result = mysqli_query($con,$sql);
if(mysqli_num_rows($result) > 0)
{
  while($row=mysqli_fetch_array($result))
  {
    array_push($response,[
      'id' => $row['id'],
      'examtitle' => $row['examtitle'],
      'examdesc' => $row['examdesc'],
      'startdate' => $row['startdate'],
      'enddate' => $row['enddate'],
      'hour' => $row['hour'],
      'minute' => $row['minute'],
      'second' => $row['second'],
      'sectionname' => $row['sectionname'],
      'totalquestion' => $row['totalquestion'],
    'totalmarks' => $row['totalmarks']
  ]);
  }
}

exit(json_encode($response));
?>
