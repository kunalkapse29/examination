<?php
include 'connection.php';
session_start();
$uid = $_SESSION['userid'];
$response = [];
$sql = "SELECT  id,examtitle, examdesc, DATE_FORMAT(startdate,'%e %b %Y') AS startdate, DATE_FORMAT(enddate,'%e %b %Y') AS enddate, hour, minute, second, UPPER(sectionname) AS sectionname, totalquestion, totalmarks, perquestionmark,flag FROM createexam WHERE userid='$uid'";
$result = mysqli_query($con,$sql);
if(mysqli_num_rows($result) > 0)
{
  while($row=mysqli_fetch_array($result))
  {
    array_push($response,[
    'id' => $row['id'],
    'examtitle' => ucwords($row['examtitle']),
    'examdesc' => ucwords($row['examdesc']),
    'startdate' => $row['startdate'],
    'enddate' => $row['enddate'],
    'hour' => $row['hour'],
    'minute' => $row['minute'],
    'second' => $row['second'],
    'sectionname' => $row['sectionname'],
    'totalquestion' => $row['totalquestion'],
    'totalmarks' => $row['totalmarks'],
    'perquestionmark' => $row['perquestionmark'],
    'flag' => $row['flag']

  ]);

  }
exit(json_encode($response));
}

?>
