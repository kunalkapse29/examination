<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
	$examid =  $_REQUEST['examid'];
  $sql ="SELECT count(id) as count1 from examattempted where examid='$examid' and studentid='$user_id'";
	$result = mysqli_query($con,$sql);
	$row=mysqli_fetch_array($result);
	$count1 =  $row['count1'];

	$sql ="SELECT examtitle,examdesc,totalquestion,totalmarks from createexam where id='$examid' and flag='1'";
	$result1 = mysqli_query($con,$sql);
	$row1=mysqli_fetch_array($result1);
	$totalquestion =  $row1['totalquestion'];
	$totalmarks =  $row1['totalmarks'];

	// $sql2 ="SELECT  GROUP_CONCAT(srno) as srno FROM testQn WHERE examid='35' and userid='1'";
	// $result2 = mysqli_query($con,$sql2);
	// $row2 =mysqli_fetch_array($result2);
	// $questionarray = $row2['srno'];
 }
if(isset($user_id) &&($count1<=0))
{
	$sql ="INSERT INTO examattempted(examid, studentid) VALUES ('$examid','$user_id')";
	$result = mysqli_query($con,$sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" >

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
     <link rel="stylesheet" type="text/css" href="jquery.countdownTimer.css" />
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs">Logout</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
        <input type="hidden" id="rowid"/>

				<input type="hidden" id="startdate"/>
				<input type="hidden" id="enddate"/>
				<input type="hidden" id="hour"/>
				<input type="hidden" id="minute"/>
				<input type="hidden" id="second"/>

        <!-- left menu starts -->
        <div class="col-sm-2 col-lg-2">
            <div class="sidebar-nav">
                <div class="nav-canvas">
                    <div class="nav-sm nav nav-stacked">

                    </div>
                    <ul class="nav nav-pills nav-stacked main-menu">

                        <li class="nav-header">
                        Remaining Time
												<!-- <div id="countdowntimer"><span id="hm_timer"><span></div>
                    <span class="timer"></span> -->
										<table style="border:0px;">
												<tr>
														<td style="width:70px;text-align:center;">Hours</td>
														<td style="width:60px;text-align:center;">Minutes</td>
														<td style="width:70px;text-align:center;">Seconds</td>
												</tr>
												<tr>
														<td colspan="4"><span id="hm_timer"></span></td>
												</tr>
										</table>
										<script>
												$(function(){
                            var hour = $("#hour").val();
														var minute = $("#minute").html();
                            // alert("minute"+minute);
														// $('#hm_timer').countdowntimer({
                            //
														// 		hours : 0,
														// 		minutes :1,
														// 		size : "lg",
														// 		timeUp : timeisUp
														// });
												});
												function timeisUp() {
				                   window.location.href="marksheet.php?examid="+param;
			 												}
										</script>
                        </li>


                    </ul>

                </div>
            </div>

            <div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well">
                            <h2><i class="glyphicon glyphicon-info-sign"></i> Total Question <span class="badge" id="counttotalquestion"></span></h2>
                        </div>
                        <div class="box-content row" id="circleshow">

                        </div>
                    </div>
                </div>
            </div>
						<div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well">
                            <h2><i class="glyphicon glyphicon-info-sign"></i> Attempted Question <span class="badge" id="countattemptquestion"></span></h2>
														<div class="box-icon">

				                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
				                            class="glyphicon glyphicon-chevron-up"></i></a>
																	</div>
												</div>
                        <div class="box-content row" id="attempted">

                        </div>
                    </div>
                </div>
            </div>
						<div class="row">
                <div class="box col-md-12">
                    <div class="box-inner">
                        <div class="box-header well">
                            <h2><i class="glyphicon glyphicon-info-sign"></i> Revisit Question <span class="badge" id="countreviewquestion"></span></h2>
														<div class="box-icon">

 			                      <a href="#" class="btn btn-minimize btn-round btn-default"><i
 			                             class="glyphicon glyphicon-chevron-up"></i></a>
												</div>
											</div>
                        <div class="box-content row" id="reviewed">

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Student Dashboard</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i>Notice</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>

                </div>
            </div>
            <div class="box-content row">
							  <div class="col-md-6">
									<input type="hidden" id="examtitle"/>
									<label>Exam Title</label>
									<span id="examtitles"></span>

								</div>
								<div class="col-md-6">
									<input type="hidden" id="examdesc"/>
									<label>Exam Description</label>
									<span id="examdescs"></span>

								</div>
								<div class="col-md-6">
									<input type="hidden" id="sectionname"/>
									<label>Section Name</label>
									<span id="sectionnames"></span>

								</div>
								<div class="col-md-6">
									<input type="hidden" id="totalquestion"/>
									<label>Total Question</label>
									<span id="totalquestions"></span>
                   <input type="hidden" id="totalmarks"/>
								</div>

                <div class="col-md-6">
                    <span class="badge" style="background-color: green;">*</span>Not Attempted Question
                </div>
								<div class="col-md-6">
										<span class="badge" style="background-color: red;">*</span>Attempted Question
								</div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Instruction</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-md-12">
                    <h3>Question</h3> <br>
                    <span id="show"></span>
                </div>

            </div>
        </div>
    </div>
</div>


    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>

		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>

<script type="text/javascript" src="jquery.countdownTimer.js"></script>

<script>
// function shuffle(arra1) {
//     var ctr = arra1.length, temp, index;
//
// // While there are elements in the array
//     while (ctr > 0) {
// // Pick a random index
//         index = Math.floor(Math.random() * ctr);
// // Decrease ctr by 1
//         ctr--;
// // And swap the last element with it
//         temp = arra1[ctr];
//         arra1[ctr] = arra1[index];
//         arra1[index] = temp;
//     }
//     return arra1;
// }
function showquestion(totalquestion){
  var str='';
	// var sectionname = "";
	// var arr = sectionname.split(",");
	// var returnarr = shuffle(arr);
	// alert(returnarr);
   $.ajax({
       url: "get_question_answer.php",
       type: "GET",
       data: ({
				examid:<?php echo $examid; ?>,
				totalquestion:totalquestion
			 }),
       success: function(data) {
				 // alert(data);
            response = JSON.parse(data);
            var count = Object.keys(response).length;
            var space =' ';
            for (var i = 0; i < count; i++) {
                var srno = response[i]['sr_no'];
                var question = response[i]['question'];

                var option1 = response[i]['option1'];

                var option2 = response[i]['option2'];
                var option3 = response[i]['option3'];
                var option4 = response[i]['option4'];
                if(i==0){
                  str=str+'<div class="row" id="'+i+'" style="display:block;"><div class="col-sm-12"><div class="form-group">'+srno+space+question+'</div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">A.<input type="radio" name="messageCheckbox'+srno+'" class="messageCheckbox'+srno+'"  value="1">'+option1+'</input></div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">B.<input type="radio" name="messageCheckbox'+srno+'" class="messageCheckbox'+srno+'"  value="2">'+option2+'</input></div></div>';

                  str=str+'<div class="col-sm-6"><div class="form-group">C.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="3">'+option3+'</input></div></div>';
  								str=str+'<div class="col-sm-6"><div class="form-group">D.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="4">'+option4+'</input></div></div>';
									str=str+'<div class="col-sm-6"><div class="form-group"></div></div><div class="col-sm-6"><div class="form-group"> <input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="5">Recheck / Revisit Again </input></div></div>';
                  str=str+'<div class="col-sm-2"><div class="form-group"><button  class="btn btn-success" id="s'+srno+'" onclick="save('+srno+','+response[i]['questionno']+','+(i+1)+')">SAVE</button></div></div>';
                  str=str+'<div class="col-sm-8"><div class="form-group"></div></div><div class="col-sm-2"><div class="form-group"><button  class="btn btn-primary" id="n'+srno+'" onclick="next('+(i+1)+')">NEXT</button></div></div></div>';
                }
                else if(i==(count-1)){
                  str=str+'<div class="row" id="'+i+'" style="display:none;"><div class="col-sm-12"><div class="form-group">'+srno+space+question+'</div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">A.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="1">'+option1+'</input></div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">B.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="2">'+option2+'</input></div></div>';

                  str=str+'<div class="col-sm-6"><div class="form-group">C.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="3">'+option3+'</input></div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">D.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="4">'+option4+'</input></div></div>';
									str=str+'<div class="col-sm-6"><div class="form-group"></div></div><div class="col-sm-6"><div class="form-group"> <input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="5">Recheck / Revisit Again </input></div></div>';
                  str=str+'<div class="col-sm-2"><div class="form-group"><button  class="btn btn-success" id="f'+srno+'" onclick="save('+srno+','+response[i]['questionno']+','+(i+1)+')">SAVE</button></div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group"></div></div><div class="col-sm-2"><div class="form-group"><button  class="btn btn-primary" id="p'+srno+'" onclick="prev('+(i-1)+')">PREV</button></div></div>';
								  str=str+'<div class="col-sm-2"><div class="form-group"><button  class="btn btn-default"  onclick="finish('+<?php echo $examid; ?>+')">END</button></div></div></div>'
								}
                else {
                  str=str+'<div class="row" id="'+i+'" style="display:none;"><div class="col-sm-12"><div class="form-group">'+srno+space+question+'</div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">A.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="1">'+option1+'</input></div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">B.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="2">'+option2+'</input></div></div>';

                  str=str+'<div class="col-sm-6"><div class="form-group">C.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="3">'+option3+'</input></div></div>';
                  str=str+'<div class="col-sm-6"><div class="form-group">D.<input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="4">'+option4+'</input></div></div>';
									str=str+'<div class="col-sm-6"><div class="form-group"></div></div><div class="col-sm-6"><div class="form-group"> <input type="radio" name="messageCheckbox'+srno+'"  class="messageCheckbox'+srno+'"  value="5">Recheck / Revisit Again </input></div></div>';
									str=str+'<div class="col-sm-2"><div class="form-group"><button  class="btn btn-success" id="s'+srno+'" onclick="save('+srno+','+response[i]['questionno']+','+(i+1)+')">SAVE</button></div></div>';
								  str=str+'<div class="col-sm-6"><div class="form-group"></div></div><div class="col-sm-2"><div class="form-group"><button  class="btn btn-primary" id="p'+srno+'" onclick="prev('+(i-1)+')">PREV</button></div></div>';

                  str=str+'<div class="col-sm-2"><div class="form-group"><button  class="btn btn-default" id="n'+srno+'" onclick="next('+(i+1)+')">NEXT</button></div></div></div>';
                }

            }

            $("#show").html(str);
          },
           error: function(data, errorThrown) {
           }
       });

}
function finish(param){
  window.location.href="marksheet.php?examid="+param;
}

function save(param,questionnumber,no){
  var examid = $("#rowid").val();
  var inputElements = document.getElementsByClassName('messageCheckbox'+param);
   var ans;
   var quesno = questionnumber;
   var attempt = 1;
   for (var i = 0; inputElements[i]; ++i) {
      if (inputElements[i].checked) {
           ans = inputElements[i].value;
         }
       }

       if(ans==undefined){
         alert("Please Select the Option");
       }
       else{
         $.ajax({
              url: "savehtmlquestionanswer.php",
              type: "POST",
              data: ({
                 ans: ans,
                 quesno: quesno,
								 questionnohtml :no,
								 examid:examid,
                 attempt: attempt

              }),
              success: function(data) {

								showattemptedquestion();
								showreviewquestions();
                    var response = JSON.parse(data);
										if(response===[]){
											alert("Answer Updated SuccessFully");
										}
										else{
											alert("Answer Saved SuccessFully");
										}
                  },
                  error: function(data, errorThrown) {

                  }
              });
       }
}

function next(param){
  $("#"+param).css("display", "block");
  $("#"+(param-1)).hide();
}
function prev(param){

  $("#"+param).css("display", "block");
  $("#"+(param+1)).hide();
}
function showquestionbyid(param,count){
	for(var i=0;i<count;i++){
		if(i===param){
			$("#"+param).show();
		}
		else{
			$("#"+i).hide();
		}

	}
}
function showcircle(totalquestion){
	var html='';

   $.ajax({
       url: "get_question_answer.php",
       type: "GET",
			 data: ({
				examid:<?php echo $examid; ?>,
				totalquestion:totalquestion
			 }),
       success: function(data) {

				 var response = JSON.parse(data);
				 var count = Object.keys(response).length;
				 $("#counttotalquestion").html(count);
				 for(var i=0;i<count;i++){
					 html +='<div class="col-md-4" id="green'+response[i]['questionno']+'">';
					 html +='<button class="btn btn-link" onclick="showquestionbyid('+i+','+count+');"><span class="badge " style="background-color: green;">'+(i+1)+'</span></button>';
					 html +='</div>';
				 }
				 $("#circleshow").append(html);
			 }
		 });
}
function showattemptedquestion(){

	  var html='';
	  $("#attempted").empty();
    $.ajax({
       url: "getattemptedquestionanswer.php",
       type: "GET",
			 data: ({examid:<?php echo $examid; ?>}),
       success: function(data) {
         // alert(data);
				 var response = JSON.parse(data);
				 var count = Object.keys(response).length;
				 $("#countattemptquestion").html(count);
				 for(var i=0;i<count;i++){
					 html +='<div class="col-md-4" id="red1'+response[i]['questionno']+'" >';
					 html +='	<button class="btn btn-link"><span class="badge" style="background-color: orange;">'+response[i]['questionno']+'</span></button>';
					 html +='</div>';
				 }
				 $("#attempted").append(html);
			 }
		 });
}
function showreviewquestions(){
	var html='';
	$("#reviewed").empty();

	$.ajax({
		 url: "getreviewedquestionanswer.php",
		 type: "GET",
		 data: ({examid:<?php echo $examid; ?>}),
		 success: function(data) {

			 var response = JSON.parse(data);
			 var count = Object.keys(response).length;
			 $("#countreviewquestion").html(count);
			 for(var i=0;i<count;i++){
				 html +='<div class="col-md-4" id="red1'+response[i]['questionno']+'" >';
				 html +='	<button class="btn btn-link" onclick="showquestionbyid('+(response[i]['questionno']-1)+','+<?php echo $totalquestion;?>+');"><span class="badge" style="background-color: blue;">'+response[i]['questionno']+'</span></button>';
				 html +='</div>';
			 }
			 $("#reviewed").append(html);
		 }
	 });
}
function showexamdetail(){

	$.ajax({
			url: "getexamdetail.php",
			type: "GET",
			data: ({examid:<?php echo $examid; ?>}),
			success: function(data) {
         var response = JSON.parse(data);
				 var count = Object.keys(response).length;
				 $('#hm_timer').countdowntimer({

							hours : response[0]['hour'],
							minutes :response[0]['minute'],
							size : "lg",
							timeUp : timeisUp
					});
				 for(var i=0;i<count;i++){

					 $('#rowid').val(response[i]['id']);
					 $('#examtitle').val(response[i]['examtitle']);
					 $('#examdesc').val(response[i]['examdesc']);
					 $('#startdate').val(response[i]['startdate']);
					 $('#enddate').val(response[i]['enddate']);
					 $('#hour').val(response[i]['hour']);
					 $('#minute').val(response[i]['minute']);
					 $('#second').val(response[i]['second']);
					 $('#sectionname').val(response[i]['sectionname']);
					 $('#totalquestion').val(response[i]['totalquestion']);
					 $('#totalmarks').val(response[i]['totalmarks']);
					 $('#examtitles').html(response[i]['examtitle']);
					 $('#examdescs').html(response[i]['examdesc']);
					 $('#sectionnames').html(response[i]['sectionname']);
					 $('#totalquestions').html(response[i]['totalquestion']);
					  showcircle(response[i]['totalquestion']);
					  showquestion(response[i]['totalquestion']);
				 }
			}
		});
}
showexamdetail();

jQuery(document).ready(function($) {


		showattemptedquestion();
		showreviewquestions();
});
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
