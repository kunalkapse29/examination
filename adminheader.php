<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.js"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-treeview/1.2.0/bootstrap-treeview.min.css" />

<div class="col-sm-2 col-lg-2">
    <div class="sidebar-nav">
        <div class="nav-canvas">
            <div class="nav-sm nav nav-stacked">

            </div>
            <ul class="nav nav-pills nav-stacked main-menu">
                <li class="nav-header">Main</li>
                <li><a class="ajax-link" href="index1.php"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a>
                </li>
                <li><a href="profile.php"><i class="glyphicon glyphicon-dashboard"></i><span> Profile</span></a>
                </li>
                <li class="accordion">
                    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Examination Section</span></a>
                    <ul class="nav nav-pills nav-stacked">
                      <li><a class="ajax-link" href="createexam.php"><i
                                  class="glyphicon glyphicon-th"></i><span> Create Exam</span></a></li>
                      <li><a class="ajax-link" href="searchexamquestion.php"><i
                                  class="glyphicon glyphicon-th"></i><span> Add Section</span></a></li>
                      <li><a class="ajax-link" href="insertexamquestion.php"><i
                                  class="glyphicon glyphicon-th"></i><span> Question Set</span></a></li>
                        <li><a class="ajax-link" href="quetionbank.php"><i
                                  class="glyphicon glyphicon-th"></i><span> Question Bank</span></a></li>
                    </ul>
                </li>
                <li class="accordion">
                    <a href="#"><i class="glyphicon glyphicon-plus"></i><span onclick="loadtree1('');"> Select Section</span></a>
                    <ul class="nav nav-pills nav-stacked" id="tree">

                    </ul>
                </li>


                <li class="accordion">
                    <a href="#"><i class="glyphicon glyphicon-plus"></i><span> Notice Section</span></a>
                    <ul class="nav nav-pills nav-stacked">
                      <li><a class="ajax-link" href="studentnotice.php"><i
                                  class="glyphicon glyphicon-th"></i><span> For Student</span></a></li>
                      <li><a class="ajax-link" href="teachernotice.php"><i
                                  class="glyphicon glyphicon-th"></i><span> For Teachers</span></a></li>
                    </ul>
                </li>
                <li><a href="student.php"><i class="glyphicon glyphicon-user"></i><span> Student Registration</span></a>
                </li>
                <li><a href="teacher.php"><i class="glyphicon glyphicon-user"></i><span> Teacher Registration</span></a>
                </li>
            </ul>

        </div>
    </div>
</div>
<script>
var a=[];
var flag1=0;
function loadtree1(param){
  if(flag1==0){
    flag1=1;
 $.ajax({
   url: "treee.php",
   method:"POST",
   data:{id:param},
   success: function(data)
   {
     var response=JSON.parse(data);
     var count=Object.keys(response).length;
     for(var i=0;i<count;i++){
  $('#tree').append('<li class="accordion"><a href="#"><i class="glyphicon glyphicon-plus"></i><span value="'+response[i]['name']+'" id="'+response[i]['id']+'" onclick="fun(this.id)">'+response[i]['name']+'</span></a></li>');
   }
 }
 });
}
}
function fun(param) {
  var param1=document.getElementById(param).innerText;
    if(!(a.includes(param)))
    {
    a.push(param);
  $.ajax({
    url: "treee.php",
    method:"POST",
    data:{id:param},
    cache:false,
    async:true,
    success: function(data)
    {
      var response=JSON.parse(data);
      var count=Object.keys(response).length;
      if(count>0){
      for(var i=0;i<count;i++){
   $('#'+param).append('<ul class="nav nav-pills nav-stacked" id="'+response[i]['id']+
   '"onclick="fun(this.id);" ><li><a class="ajax-link"><i class="glyphicon glyphicon-plus"></i><span>'+response[i]['name']+'</span></a></li></ul>');
    }
      }
  else {
    $('#'+param).html('<ul class="nav nav-pills nav-stacked"><li><a class="ajax-link"><i class="glyphicon glyphicon-th"></i><span id="'
    +param+'" >'+param1+'</span></a></li></ul>');
    document.location.href = 'sectionwise.php?section='+param1;
  }
  }
  });
}
}
</script>
