<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
  $examid =  $_REQUEST['examid'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs">Logout</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <!-- left menu starts -->
       <?php  include 'studentheader.php' ?>
        <!--/span-->


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Marksheet</a>
        </li>
    </ul>
</div>


<div class="row">
  <div class="box col-md-4">
    <div class="box-inner">
        <div class="box-header well">
            <h2><i class="glyphicon glyphicon-info-sign"></i>Student Profile</h2>

            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round btn-default"><i
                        class="glyphicon glyphicon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round btn-default"><i
                        class="glyphicon glyphicon-chevron-up"></i></a>
                <!-- <a href="#" class="btn btn-close btn-round btn-default"><i
                        class="glyphicon glyphicon-remove"></i></a> -->
            </div>
        </div>
        <div class="box-content row">
            <div class="col-md-12" id="studentprofile">


            </div>

        </div>
    </div>
  </div>
    <div class="box col-md-4">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i>Result</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <!-- <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a> -->
                </div>
            </div>
            <div class="box-content row" id="resultprofile">

            </div>
        </div>
    </div>
    <div class="box col-md-4">

        <div class="box-content row">
          <div class="col-md-12">
      <!-- <img src="result.jpg"></img> -->
      </div>
      </div>
    </div>
</div>




    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>


    <footer class="row">
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://usman.it" target="_blank">Muhammad
                Usman</a> 2012 - 2015</p>

        <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Powered by: <a
                href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
    </footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script>


function showstudentprofile(){
	var html ='';
	$.ajax({
			 url: "getshowstudentprofile.php",
			 type: "GET",
			 success: function(data) {
            // alert(data);
						response = JSON.parse(data);
            html +='<div class="row"><div class="col-sm-6"><b>First Name:</b> '+response['firstname']+'</div><div class="col-sm-6"><b>Last Name:</b>'+response['lastname']+'</div></div>';
						html +='<div class="row"><div class="col-sm-6"><b>Gender:</b>'+response['gender']+'</div><div class="col-sm-6"><b>Mobile No:</b>'+response['mobileno']+'</div></div>';
						html +='<div class="row"><div class="col-sm-6"><b>Email Address:</b>'+response['email']+'</div><div class="col-sm-6"><b>Date-of-Birth:</b>'+response['dateofbirth']+'</div></div>';
					  $("#studentprofile").html(html);
					},
					 error: function(data, errorThrown) {
					 }
			 });
}
function showstudentresult(){
	var html ='';
	$.ajax({
			 url: "getshowstudentresult.php",
			 type: "GET",
       data: ({examid:<?php echo $examid; ?>}),
			 success: function(data) {
            // alert(data);
            var response = JSON.parse(data);

           html+='<div class="col-md-12">';
          html+=' <h3> '+response['examtitle']+'</h3>';
          html+=' <h4>'+response['examdesc']+'</h4>';

           html+='</div>';
           html+='<div class="col-md-6">';
             html+='Start Date :'+response['startdate']+'';
           html+='</div>';
           html+='<div class="col-md-6">';
          html+='   End Date :  '+response['enddate']+'';
           html+='</div>';
           html+='<div class="col-md-6">';
          html+='   Paper Name : '+response['sectionname']+'';
          html+=' </div>';
					html+=' <div class="col-md-6">';
          html+='   Total Marks : '+response['totalquestion']+'';
          html+=' </div>';
					html+='<div class="col-md-12">';
				 html+='   Per Question Marks : '+response['perquestionmark']+'';
				 html+=' </div>';

          if((((response['correctanswer']*response['perquestionmark'])/response['totalmarks'])*100).toFixed(2)>60){
            html+=' <div class="col-md-12">';
            html+='<h3><font color="green">Yor are Passed</font></h3><h3><font color="green">Percentage   '+(((response['correctanswer']*response['perquestionmark'])/response['totalmarks'])*100).toFixed(2)+' %</font></h3>'
            html+=' </div>';
            html+=' <div class="col-md-12">';
            html+='     <marquee><h3><font color="gray">Your Score is : '+(response['correctanswer']*response['perquestionmark'])+'/'+response['totalmarks']+' Marks</font></h3></marquee>';
            html+=' </div>';
            html+=' <div class="col-md-12">';
            html+=' <h3><font color="green">Congratulations</font></h3>';
            html+=' <p>Competitive exams are not so easy but you do it with competency and had a brilliant result, many Many Congratulations!</p>';
            html+=' </div>';
          }
          else{
            html+=' <div class="col-md-12">';
            html+='<h3><font color="red">Yor are not Qualified This Exam</font></h3><h3><font color="green">Percentage   '+(((response['correctanswer']*response['perquestionmark'])/response['totalmarks'])*100).toFixed(2)+' %</font></h3>'
            html+=' </div>';
            html+=' <div class="col-md-12">';
            html+='     <marquee><h3><font color="gray">Your Score is : '+(response['correctanswer']*response['perquestionmark'])+'/'+response['totalmarks']+' Marks</font></h3></marquee>';
            html+=' </div>';
            html+=' <div class="col-md-12">';
            html+=' <h3><font color="red">Try Again Next Time</font></h3>';
            html+=' <p>Competitive exams are not so easy but you do it with competency and had a brilliant result, many Many Congratulations!</p>';
            html+=' </div>';
              html+=' <div class="col-md-12">';
            html+='<p>Thanks,</p>';
html+='<p>HR Team</p>';
html+='<p>Kunal Kapse</p>';
html+='<p>Office:9766695099</p>';
html+='<p>Email: krkunal29@gmail.com</p>';
            html+=' </div>';
          }

          $("#resultprofile").html(html);
					},
					 error: function(data, errorThrown) {
					 }
			 });
}
jQuery(document).ready(function($) {
showstudentprofile();
showstudentresult();
});
</script>


</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
