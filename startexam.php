<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs">Logout</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <!-- left menu starts -->
       <?php  include 'studentheader.php' ?>
        <!--/span-->


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Student Dashboard</a>
        </li>
    </ul>
</div>


<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Instruction</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-md-12">
									<h3>Instructions to Candidates</h3>
									<p>(a)   DO NOT OPEN THIS TEST BOOK UNTIL THE SUPERVISOR TELLS YOU TO START. </p>
									<p>(b)   This is a multiple–choice test.   You will have 15 minutes to complete the test.</p>
									<p>(c)   Each question has 4 answer choices, A, B, C or D.  You must mark your choice in pencil on your answer sheet. </p>
									<p>(d)   During the test use a rubber eraser to erase any mistakes on the answer sheet.  Do not cross outanswers, as this means the question will automatically be scored as wrong.</p>
									<p>(e)   Mark only one answer per question – if you mark more than one answer for a question (for example both A and C) it will automatically be scored as wrong.</p>
									<p>(f)    Do not make any other marks on the answer sheet, as this could accidentally affect your score.</p>
                  <p>(g)   <font color="red">Please Don't Refresh Or Reload Page Or Open New Tab . Your Test Will Be cancelled automatically</font>.</p>
									<p>(h)   <font color="green">If You are ready for Test click on Live Test</font> .</p>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="box col-md-4">
			<div class="box-inner">
					<div class="box-header well" data-original-title="">
							<h2><i class="glyphicon glyphicon-list"></i> Live Test</h2>

							<div class="box-icon">
									<a href="#" class="btn btn-setting btn-round btn-default"><i
													class="glyphicon glyphicon-cog"></i></a>
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
													class="glyphicon glyphicon-chevron-up"></i></a>
									<a href="#" class="btn btn-close btn-round btn-default"><i
													class="glyphicon glyphicon-remove"></i></a>
							</div>
					</div>
					<div class="box-content" id="liveevents">



					</div>
			</div>
	</div>
    <!-- <div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Start Exam</h2> -->

                <!-- <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div> -->
            <!-- </div>
            <div class="box-content row">
                <div class="col-md-12">
									<button type="button" class="btn btn-success" onclick="startexam(40)">START EXAM</button>
                </div>
            </div>
        </div> -->
    <!-- </div> -->
</div>



    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>


		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script>

function liveevents(){
	$("#liveevents").empty();
	var html ='';
  $.ajax({
 		 url: "getliveevent.php",
 		 type: "GET",
 		 success: function(data) {
 			 var response = JSON.parse(data);
 			 var count = Object.keys(response).length;
 			 for(var i=0;i<count;i++){
 				 html +='<a href="exampaper1.php?examid='+response[i]['id']+'"><div class="row"><div class="col-sm-12" >'+(i+1)+'.'+response[i]['examtitle'].toUpperCase()+'';
 				 html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" > Exam Description :'+response[i]['examdesc']+'';
					html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12"><b> '+response[i]['startdate']+' - '+response[i]['enddate']+'</b><br/>Duration '+response[i]['hour']+':'+response[i]['minute']+':'+response[i]['second']+'';
 				 html +='</div></div></a></br>';
 			 }


 			 $("#liveevents").append(html);
 		 }
 	 });
}
jQuery(document).ready(function($) {
    liveevents();

});
</script>


</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
