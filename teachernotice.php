<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
  	<link href='css/editor.css' rel='stylesheet'>
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="select/select2.min.css"  />
    <script src="select/select2.min.js" type="text/javascript" ></script>
    <link rel="shortcut icon" href="img/favicon.ico">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body>

    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <?php include 'adminheader.php'; ?>


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Teacher Notice</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i>Teacher Notice</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content buttons">
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Teacher Email</label>
                    <select name="teacheremail" id="teacheremail"  class="form-control select2-select" style="width:100%;" data-live-search="true"  autocomplete="off" required/ >
                      <option values="">  </option>
                    </select>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Exam Title</label>
                  <select name="examtitle" id="examtitle"  class="form-control select2-select" style="width:100%;" data-live-search="true"  autocomplete="off" required/ >
                    <option values="">  </option>
                  </select>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Notice</label>
                  <div id="msg"></div>
              </div>

              </div>
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Start Date<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="date"  id="startdate" style="width:100%;"></input>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label" >End Date<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="date"  id="enddate" style="width:100%;"></input>
              </div>

              </div>
              <div class="row">
                <div class="form-group col-md-12">
      						<textarea id="txteditor"></textarea>
      						<span id="selecttexteditor"></span>
      					</div>
              </div>

              <div class="row">
              <div class="form-group has-success col-md-4">
                  <button class="btn btn-success" onclick="saveteachernotice()">Submit</button>

              </div>
              </div>

            </div>
        </div>
    </div>

</div>







    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>

		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script src="js/editor.js"></script>
<script>
function saveteachernotice(){
	var teacheremail = $("#teacheremail").val();
  var examtitle = $("#examtitle").val();
  var startdate = $("#startdate").val();
  var enddate = $("#enddate").val();
	var txteditor = $("#txteditor").Editor("getText");
  // alert(txteditor);
  if(startdate=="" || enddate==""){
   alert("All Fields is required");
  }
  else{
	if(txteditor==""){
   $("#selecttexteditor").html("<font color='red'>Select TextEditor Value</font>");
	}
	else{
		$("#selecttexteditor").html("");
	 $.ajax({
		type:'POST',
		url:'saveteachernotice.php',
		data:({
      teacheremail:teacheremail,
      examtitle:examtitle,
      startdate:startdate,
      enddate:enddate,
      txteditor:txteditor
		}),
	  success:function(msg){
      alert("saved");
      window.location.reload();
		}
	});
}
}

}
function getmsg(){
  var msg;
  msg = '<div class="alert alert-success">';
  msg += '    <button type="button" class="close" data-dismiss="alert">&times;</button>';
  msg += '    <strong>Information!</strong><br/>If you not select teacher email. Then notice goes all teachers';
  msg += '</div>';
  $("#msg").append(msg);
}
function getteacheremail(){

      $.ajax({
                type: "POST",
                url: "getteacheremail.php",
                data:'getrow',
                success: function(msg)
                {

                  $("#teacheremail").html(msg);

                }
              });
}
function getexamtitle(){

      $.ajax({
                type: "POST",
                url: "getexamtitle.php",
                data:'getrow',
                success: function(msg)
                {

                  $("#examtitle").html(msg);

                }
              });
}
$(document).ready(function(){
getmsg();
getteacheremail();
getexamtitle();
	 $("#txteditor").Editor();
   $("#teacheremail").select2({
     allowClear: true,
     placeholder: "Select Student Email"
   });
   $("#examtitle").select2({
     allowClear: true,
     placeholder: "Select Exam Title"
   });

});

</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
