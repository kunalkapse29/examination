<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
  	<link href='css/editor.css' rel='stylesheet'>
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

</head>

<body>
	 <!-- instruction and notice modal -->
	<div id="instructionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Instruction / Notice</h4>
				<input type="hidden" id="instructionid" />
      </div>
      <div class="modal-body">
				<div class="row">
					<div class="col-lg-12 nopadding">
						<textarea id="txteditor"></textarea>
						<span id="selecttexteditor"></span>
					</div>
				</div>
      </div>
      <div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="saveinstruction()">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
 </div>
 <!--instruction and notice modal close  -->
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <?php include 'employeeheader.php'; ?>


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Create Exam</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i> Create Exam</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content buttons">
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Exam Title<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="text" class="form-control" id="examtitle"></input>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Exam Desc<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="text" class="form-control"  id="examdesc"></input>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Section Name<sup><font color="red" size="3px;">*</font></sup></label>
                  <span  id="sectionhtml"></span>
              </div>

              </div>
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Start Date<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="date"  id="startdate" style="width:100%;"></input>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label" >End Date<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="date"  id="enddate" style="width:100%;"></input>
              </div>

              </div>
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Total Question<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="number"  id="totalquestion" style="width:100%;"></input>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label" >Total Marks<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="number"  id="totalmarks" style="width:100%;"></input>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Mark / Question<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="number"  id="perquestionmark" style="width:100%;"></input>
              </div>
              </div>
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Hour<sup><font color="red" size="3px;">*</font></sup></label>
                    <select type="text"  id="hour"  data-rel="chosen">
                      <option value="">---Select Hour---</option>
                      <option value="0">0</option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                    </select>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label" >Minute<sup><font color="red" size="3px;">*</font></sup></label>
                  <select type="text"  id="minute" data-rel="chosen">
                    <option value="">---Select Minute---</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                    <option value="41">41</option>
                    <option value="42">42</option>
                    <option value="43">43</option>
                    <option value="44">44</option>
                    <option value="45">45</option>
                    <option value="46">46</option>
                    <option value="47">47</option>
                    <option value="48">48</option>
                    <option value="49">49</option>
                    <option value="50">50</option>
                    <option value="51">51</option>
                    <option value="52">52</option>
                    <option value="53">53</option>
                    <option value="54">54</option>
                    <option value="55">55</option>
                    <option value="56">56</option>
                    <option value="57">57</option>
                    <option value="58">58</option>
                    <option value="59">59</option>
                  </select>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Second<sup><font color="red" size="3px;">*</font></sup></label>
                  <select type="text"  id="second" data-rel="chosen">
                    <option value="">---Select Second---</option>
                    <option value="0">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                    <option value="31">31</option>
                    <option value="32">32</option>
                    <option value="33">33</option>
                    <option value="34">34</option>
                    <option value="35">35</option>
                    <option value="36">36</option>
                    <option value="37">37</option>
                    <option value="38">38</option>
                    <option value="39">39</option>
                    <option value="40">40</option>
                    <option value="41">41</option>
                    <option value="42">42</option>
                    <option value="43">43</option>
                    <option value="44">44</option>
                    <option value="45">45</option>
                    <option value="46">46</option>
                    <option value="47">47</option>
                    <option value="48">48</option>
                    <option value="49">49</option>
                    <option value="50">50</option>
                    <option value="51">51</option>
                    <option value="52">52</option>
                    <option value="53">53</option>
                    <option value="54">54</option>
                    <option value="55">55</option>
                    <option value="56">56</option>
                    <option value="57">57</option>
                    <option value="58">58</option>
                    <option value="59">59</option>
                  </select>
              </div>
              </div>
              <div class="row">
              <div class="form-group has-success col-md-4">
                  <button class="btn btn-success" onclick="savecreateexam()">Submit</button>

              </div>
              </div>

            </div>
        </div>
    </div>

</div>

<div class="row">
<div class="box col-md-12">
<div class="box-inner">
<div class="box-header well" data-original-title="">
    <h2><i class="glyphicon glyphicon-user"></i>Exam Schedule</h2>

    <div class="box-icon">
        <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                class="glyphicon glyphicon-chevron-up"></i></a>
        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
    </div>
</div>
<div class="box-content">

<div class="col-md-12">
  <label class="control-label" for="inputWarning1">&nbsp;<sup>
</div>
<table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
<thead>
<tr>
    <th>No</th>
    <th>Exam Title</th>
    <th>Exam Description</th>
    <th>Section</th>
    <th>Start Date</th>
    <th>End Date</th>
    <th>Duration  <br/><font color="red" size="2">(hh:mm:ss)</font></th>
    <th>Total Question</th>
    <th>Each Question Marks</th>
    <th>Total Marks</th>
		<th>Instruction</th>
		<th>Status</th>
    <th>Actions</th>
</tr>
</thead>
<tbody id="fetchcellvalue">


</tbody>
</table>
</div>
</div>
</div>
<!--/span-->

</div><!--/row-->






    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>

		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script src="js/editor.js"></script>
<script>

function getsectionname(){
   var html ='';
  $("#sectionhtml").empty();
  $.ajax({
       type:'POST',
       url:'getsectionname.php',
       success:function(msg){

               var response = JSON.parse(msg);
               var count = Object.keys(response).length;
               html='<select  class="control-label" id="section" data-rel="chosen" style="width:100%;"><option value="">Select Here</option>';
               for(var i=0;i<count;i++){
                  html +='<option value="'+response[i]['sectionname']+'">'+response[i]['sectionname']+'</option>';
               }
               html+='</select>';

              $("#sectionhtml").html(html);
       }

  });
}
// setsectionname();
function saveinstruction(){
	var id = $("#instructionid").val();
	var txteditor = $("#txteditor").Editor("getText");
	if(txteditor==""){
   $("#selecttexteditor").html("<font color='red'>Select TextEditor Value</font>");
	}
	else{
		$("#selecttexteditor").html("");
		$('#instructionModal').modal('hide');
	$.ajax({
		type:'POST',
		url:'saveinstructionnotice.php',
		data:({
			id:id,
			txteditor:txteditor
		}),
	  success:function(msg){
       // $("#instructionModal .close").modal();
		}
	});
}

}
$(document).ready(function(){
  getsectionname();
   searchques();
	 $("#txteditor").Editor();
});
function savecreateexam(){
  var examtitle = $("#examtitle").val();
  var examdesc = $("#examdesc").val();
  var startdate = $("#startdate").val();
  var enddate = $("#enddate").val();
  var hour = $("#hour").val();
  var minute = $("#minute").val();
  var second = $("#second").val();
  var sectionname = $("#section").val();
  var totalquestion = $("#totalquestion").val();
  var totalmarks = $("#totalmarks").val();
  var perquestionmark = $("#perquestionmark").val();
  if(sectionname=="" || examtitle=="" || examdesc==""|| startdate=="" || enddate=="" || hour=="" || minute=="" || second=="" || totalquestion=="" || totalmarks==""  ){
     alert("All Fields are Required");
  }
  else {
    $.ajax({
      type:'POST',
      url:'savecreateexam.php',
      data:({
        examtitle:examtitle,
        examdesc:examdesc,
        startdate:startdate,
        enddate:enddate,
        hour:hour,
        minute:minute,
        second:second,
        sectionname:sectionname,
        totalquestion:totalquestion,
        totalmarks:totalmarks,
        perquestionmark:perquestionmark

      }),
      success:function(msg){
      var response = JSON.parse(msg);
      if(response){
         alert("Exam Created");
				 searchques();
      }
      }
    });
  }
}
function removesection(param){
  $.ajax({
    type:'POST',
    url:'removesectionbyid.php',
    data:({sectionid:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        alert("Section Name removed SuccessFully");
        window.location.reload();
      }
    }
  });
}
function removeid(param){

  $.ajax({
    type:'POST',
    url:'removecreateexambyid.php',
    data:({createexam:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        alert("Create Exam removed SuccessFully");
      searchques();
      }
    }
  });
}
function active(param){
	$.ajax({
    type:'POST',
    url:'activeexambyid.php',
    data:({createexamid:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        searchques();
      }
    }
  });
}
function inactive(param){
	$.ajax({
    type:'POST',
    url:'inactiveexambyid.php',
    data:({createexamid:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        searchques();
      }
    }
  });
}
function openmodal(param){
	$("#instructionid").val(param);
}
function searchques(){
  $("#fetchcellvalue").empty();
  $.ajax({
    type:'POST',
    url:'searchcreateexam.php',
    success:function(msg){
      var html ='';
        var response = JSON.parse(msg);
        var count = Object.keys(response).length;
        for(var i=0;i<count;i++){

        html +='<tr>';
        html +='<td style="text-align: center;">'+(i+1)+'</td>';
        html +='<td style="text-align: center;">'+response[i]['examtitle']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['examdesc']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['sectionname']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['startdate']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['enddate']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['hour']+'-'+response[i]['minute']+'-'+response[i]['second']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['totalquestion']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['perquestionmark']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['totalmarks']+'</td>';
				html +='<td style="text-align: center;"><button type="button" class="btn btn-link" data-toggle="modal" data-target="#instructionModal" onclick="openmodal('+response[i]['id']+')"><span class="label-success label label-default" >ADD</span></button></td>';
				if(response[i]['flag']==0){
				html +='<td class="center">';
				html +='<button type="button" class="btn btn-link" onclick="active('+response[i]['id']+')"><span class="label-success label label-default" >Active</span></button>';
				html +='</td>';
				}
				else{
				html +='<td class="center">';
	      html +='<button type="button" class="btn btn-link" onclick="inactive('+response[i]['id']+')"><span class="label-default label">Inactive</span></button>';
	      html +='</td>';
				}

        html +='<td style="text-align: center;">';
        html +='<button class="btn btn-danger" onclick="removeid('+response[i]['id']+')">';
        html +='<i class="glyphicon glyphicon-trash icon-white"></i></button>';
        html +='      </td>';
        html +='</tr>';
        }
        $("#fetchcellvalue").append(html);

    }
  });
}
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
