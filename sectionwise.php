<?php
session_start();
include 'connection.php';
$section=$_REQUEST['section'];
if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <link href="select2/select2-bootstrap.min.css" rel="stylesheet" />
		<link href="select2/select4.css" rel="stylesheet" />
		<script src="select2/select4.js" type="text/javascript"></script>
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>

    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <!-- left menu starts -->
        <?php include 'adminheader.php'; ?>
        <!--/span-->
        <!-- left menu ends -->


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
                <div>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Quetion Bank</a>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-th"></i> QUETION BANK</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-8">


                        </div>
                         <div class="col-md-4">

                            <label class="control-label" for="inputWarning1">Test <sup>
                              <font color="red" size="3px;">*</font></sup></label>&nbsp;&nbsp;
                          <select  class="select2-select form-control" id="test" name="test" required>
                          <option value="">---Select test---</option>

                          </select>
                        </div>
                    </div><br>

                    <div id="div1">

                    </div>
                  <br/>

                </div>
            </div>
        </div>
        <!--/span-->
    </div><!--/row-->


    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->



    <hr>



		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script type="text/javascript">

    function clearfunction(){
      var form = document.getElementById("questionmodule");
      form.reset();
    }
    function funclear(param){
      $('.messageCheckbox'+param).prop("checked", false);
    }

 function opentextfield(){

     $("#enterquestionno").html("<strong>Enter quetion No</strong>");
     document.getElementById("questionid").style.display="block";
 }
 function questionno(param){

      $.ajax({
           url: "getquestionanswer.php",
           type: "GET",
           data: ({
              questionno: param
           }),
           success: function(data) {
                response = JSON.parse(data);

                $("#question").val(response['question']);
                $("#option1").val(response['option1']);
                $("#option2").val(response['option2']);
                $("#option3").val(response['option3']);
                $("#option4").val(response['option4']);

              },
               error: function(data, errorThrown) {
               }
           });
 }
 $(document).ready(function(){
display_qnbank();
display_test();
                            $('select').select2({
									 allowClear: true,
									 placeholder: "Select here",
							 });
 });
 function display_qnbank() {
    $("#div1").html("");
		var param='<?php echo $section ?>';
    $.ajax({
           url: "display_qnbank.php",
           type: "GET",
           data: ({
              section:param
           }),
           success: function(data) {
            var response = JSON.parse(data);
            var count = Object.keys(response).length;
            for (var i = 0; i < count; i++) {
                var chk1=chk2=chk3=chk4=used="";
                if(response[i]['correctoption']=='1')
                {
                     chk1='<i class="fa fa-check" style="color:green"></i>';
                }
                if(response[i]['correctoption']=='2')
                {
                     chk2='<i class="fa fa-check" style="color:green"></i>';
                }
                if(response[i]['correctoption']=='3')
                {
                     chk3='<i class="fa fa-check" style="color:green"></i>';
                }
                if(response[i]['correctoption']=='4')
                {
                     chk4='<i class="fa fa-check" style="color:green"></i>';
                }
                used=response[i]['used'];
                if(!(used))
                {
                    used="Unused Quetion";
                    }
                $("#div1").append('<div class="row"><div class="col-md-12"><strong>Quetion </strong>'+response[i]['quetion']+
                '</div></div><div class="row"><div class="col-md-2">'+chk1+'<strong>Option 1</strong> '+response[i]['option1']+'</div><div class="col-md-2">'+chk2+'<strong>Option 2 </strong>'
                +response[i]['option2']+'</div><div class="col-md-2">'+chk3+'<strong>Option 3</strong> '+response[i]['option3']+'</div><div class="col-md-2">'+chk4+'<strong> Option 4 </strong>'
                +response[i]['option4']+'</div><div class="col-md-2"><input type="checkbox" id="mycheckbox" value="'+response[i]['srno']+'" required></div><div class="col-md-2"><a style="color:orange"  data-toggle="collapse" data-target="#'+i+'">usedIn</a><div id="'+i+'" class="collapse">'+used+'</div></div></div><hr>');

            }
            $("#div1").append('<div class="text-center"><button onclick="qns()" type="button" class="btn btn-primary">Submit</button><button type="button" class="btn btn-default" onclick="window.location.reload();">Cancel</button></div>');
              },
               error: function(data, errorThrown) {
               }
           });
 }
function qns()
{
    var tstname=$("#test").val();
    var qnslist = "";
            $.each($("input[id='mycheckbox']:checked"), function(){
                qnslist+=$(this).val()+",";
            });
            if(!(qnslist))
            {
                alert("plz select quetions");
            }
            if(!(tstname))
            {
                alert("select test");
            }
            // alert(qnslist);
            else{
            $.ajax({
           url: "addtotest.php",
           type: "GET",
           data: {qnlist:qnslist,tstname:tstname},
           success: function(data) {
               var response=JSON.parse(data);
               if(response['true'])
               window.location.reload();
           }
            });
            }
}
function display_test()
{
    $.ajax({
           url: "display_test.php",
           type: "GET",
           success: function(data) {
            //    alert(data);
               $("#test").html(data);
           }
            });
}
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
