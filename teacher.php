<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
		<link rel="stylesheet" href="select2/datatablebootstrap4.css">
    <link rel="shortcut icon" href="img/favicon.ico">
		<link href="select2/select2-bootstrap.min.css" rel="stylesheet" />
		<link href="select2/select4.css" rel="stylesheet" />
		<script src="select2/select4.js" type="text/javascript"></script>

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <!-- left menu starts -->
        <?php include 'adminheader.php'; ?>
        <!--/span-->
        <!-- left menu ends -->


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Teacher/Staff Registration</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="col-md-12">
      <div id="msg"></div>
    </div>
</div>

<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> Personal Details</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-md-12">
                  <div class="form-group has-success col-md-4">
                      <label class="control-label" for="inputSuccess1">Title</label>
                      <!-- <input type="text" class="form-control" id="inputSuccess1"> -->
                      <select id="title" class="form-control select2-select" style="width:100%;">
                      <option value="">---Select Title---</option>
                      <option value="Mr.">Mr.</option>
                      <option value="Mrs.">Mrs.</option>
                      <option value="Ms.">Ms.</option>
                      <option value="Prof.">Prof.</option>
                      <option value="Dr.">Dr.</option>
                      </select>
                  </div>


                </div>
                <div class="col-md-12">
                <div class="form-group has-success col-md-4">
                    <label class="control-label" for="inputWarning1">First Name <sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="text" class="form-control" id="firstname">
                </div>
                <div class="form-group has-success col-md-4">
                    <label class="control-label" for="inputError1">Middle Name</label>
                    <input type="text" class="form-control" id="middlename">
                </div>
                <div class="form-group has-success col-md-4">
                    <label class="control-label" for="inputWarning1">Last Name<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="text" class="form-control" id="lastname">
                </div>
              </div>
              <div class="col-md-12">
								<div class="form-group has-success col-md-4">
		                <label class="control-label" for="inputWarning1">Date of Birth <sup><font color="red" size="3px;">*</font></sup></label>
		                  <input type="date" class="form-control" id="dateofbirth"/>
		            </div>
              <div class="form-group has-success col-md-4">
                  <label class="control-label" for="inputWarning1">Mobile No<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="text" class="form-control" id="mobileno">
              </div>
              <div class="form-group has-success col-md-4">
                  <label class="control-label" for="inputError1">Email/Login Id<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="text" class="form-control" id="email">
              </div>

            </div>
            <div class="col-md-12">

            <div class="form-group has-success col-md-4">
                <label class="control-label" for="inputWarning1">Qualification<sup><font color="red" size="3px;">*</font></sup></label>
                <input type="text" class="form-control" id="acategory">

            </div>
            <div class="form-group has-success col-md-4">
                <label class="control-label" for="inputError1">Nationality<sup><font color="red" size="3px;">*</font></sup></label>
                <select id="nationality" class="form-control select2-select" style="width:100%;" >
                <option value="">---Select Nationality---</option>
                <option value="Domestic">Domestic</option>
                <option value="Overseas">Overseas</option>
                </select>

            </div>
						<div class="form-group has-success col-md-4">
								<label class="control-label" for="inputWarning1">Gender <sup><font color="red" size="3px;">*</font></sup></label>
								<br/>
								<label class="radio-inline"><input type="radio" name="gender" id="genderM" value="Male" checked>Male</label>
								<label class="radio-inline"><input type="radio" name="gender" id="genderF" value="Female">Female</label>
								<label class="radio-inline"><input type="radio" name="gender" id="genderO" value="Other">Other</label>
						</div>

          </div>
            <div class="col-md-12">
        <h5><i class="glyphicon glyphicon-info-sign"></i> <label>Academic Details</label> </h5>
        </div>
        <div class="col-md-12">
        <div class="form-group has-success col-md-4">
            <label class="control-label" for="inputWarning1">Experience(In Years)</label>
              <input type="number" class="form-control" id="grno"/>
        </div>
        <div class="form-group has-success col-md-4">
            <label class="control-label" for="inputWarning1">Admission Year<sup><font color="red" size="3px;">*</font></sup></label>
            <select id="addmissionyear" class="form-control select2-select" style="width:100%;">
            <option value="">---Select Year---</option>
            <option value="2018-19">2018-19</option>
            <option value="2019-20">2019-20</option>
            </select>
        </div>
        <div class="form-group has-success col-md-4">
            <label class="control-label" for="inputError1">Academic Year<sup><font color="red" size="3px;">*</font></sup></label>
            <select id="academicyear" class="form-control select2-select" style="width:100%;">
            <option value="">---Select Year---</option>
            <option value="2018-19">2018-19</option>
            <option value="2019-20">2019-20</option>
            </select>

        </div>

      </div>
      <div class="col-md-12">
      <div class="form-group has-success col-md-4">
          <label class="control-label" for="inputWarning1">Course.<sup><font color="red" size="3px;">*</font></sup></label>
            <input type="text" class="form-control" id="course"/>
      </div>
      <div class="form-group has-success col-md-4">
          <label class="control-label" for="inputWarning1">Section<sup><font color="red" size="3px;">*</font></sup></label>
          <input type="text" class="form-control" id="section"/>
      </div>
      <div class="form-group has-success col-md-4">
          <label class="control-label" for="inputError1">Date of Enrolment<sup><font color="red" size="3px;">*</font></sup></label>
    <input type="date" class="form-control" id="dateofenroll"/>

      </div>

    </div>

    <div class="col-md-12">
    <div class="form-group has-success col-md-4">
        <button class="btn btn-primary" onclick="saveteacherinfo()">Save</button>
    </div>
    <div class="form-group has-success col-md-4">
  <!-- <button class="btn btn-success">Reset</button> -->
    </div>
    <div class="form-group has-success col-md-4">

    </div>

  </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
<div class="box col-md-12">
<div class="box-inner">
<div class="box-header well" data-original-title="">
    <h2><i class="glyphicon glyphicon-user"></i>Teachers Information</h2>

    <div class="box-icon">
        <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                class="glyphicon glyphicon-chevron-up"></i></a>
        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
    </div>
</div>
<div class="box-content" style="overflow:auto;">


<table class="table table-striped table-bordered" id="teachers">
<thead>
<tr>
    <th style="text-align: center;">No</th>
    <th style="text-align: center;">First Name</th>
    <th style="text-align: center;">Mobile No</th>
    <th style="text-align: center;">Email</th>
    <th style="text-align: center;">Actions</th>
</tr>
</thead>
<tbody id="fetchcellvalue">


</tbody>
</table>
</div>
</div>
</div>
<!--/span-->

</div><!--/row-->


    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>
		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src="js/jquerydatatablemin.js"></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<link rel="stylesheet" href="assets/stylesheets/datatables/CSS/buttons.bootstrap4.min.css" />
<link rel="stylesheet" href="assets/stylesheets/datatables/CSS/dataTables.bootstrap4.min.css" />

<link rel="stylesheet" href="assets/stylesheets/datatables/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="assets/stylesheets/datatables/css/buttons.dataTables.min.css">
<script src="assets/stylesheets/datatables/jquery.dataTables.min.js"></script>
<script src="assets/stylesheets/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/stylesheets/datatables/dataTables.buttons.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/stylesheets/datatables/jszip.min.js"></script>
<script src="assets/stylesheets/datatables/pdfmake.min.js"></script>
<script src="assets/stylesheets/datatables/vfs_fonts.js"></script>
<script src="assets/stylesheets/datatables/buttons.html5.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.print.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.colVis.min.js"></script>
<script>
$(document).ready(function(){
	getteacherinfo();
	$('select').select2({
			allowClear: true,
			placeholder: "Select here",
	});
});
function getteacherinfo(){
  $("#fetchcellvalue").empty();
  $.ajax({
    type:'POST',
    url:'getteachersinfo.php',
    success:function(msg){

      var html ='';
        var response = JSON.parse(msg);
        var count = Object.keys(response).length;
        for(var i=0;i<count;i++){

					html +='<tr>';
	        html +='<td style="text-align: center;">'+(i+1)+'</td>';
	        html +='<td style="text-align: center;">'+response[i]['firstname']+' '+response[i]['lastname']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['lastname']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['gender']+'</td>';
	        html +='<td style="text-align: center;">'+response[i]['mobileno']+'</td>';
	        html +='<td style="text-align: center;">'+response[i]['email']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['dateofbirth']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['acategory']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['nationality']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['grno']+'</td>';
          //
					// html +='<td style="text-align: center;">'+response[i]['addmissionyear']+'</td>';
					// html +='<td style="text-align: center;">'+response[i]['academicyear']+'</td>';
					// html +='<td style="text-align: center;">'+response[i]['course']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['section']+'</td>';
	        // html +='<td style="text-align: center;">'+response[i]['dateofenroll']+'</td>';
	        html +='<td style="text-align: center;">';
					html +='<a href="editteacherinfo.php?id='+response[i]['id']+'"><button type="button" class="btn btn-primary" title="For Edit"  ><i class="glyphicon glyphicon-edit"></i></button></a>';
	        html +='<button class="btn btn-danger" onclick="removeid('+response[i]['id']+')">';
	        html +='<i class="glyphicon glyphicon-trash icon-white"></i></button>';
	        html +='      </td>';
	        html +='</tr>';
        }
        $("#fetchcellvalue").append(html);
				// $('#teachers').DataTable();
				var table = $('#teachers').DataTable( {
				lengthChange: false,
				buttons: [ 'copy', 'excel', 'csv', 'pdf']
				} );
				table.buttons().container()
				.appendTo( '#teachers_wrapper .col-md-6:eq(0)' );
    }
  });
}
function removeid(param){

  $.ajax({
    type:'POST',
    url:'removeteacherbyid.php',
    data:({teacherid:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){

      // getteacherinfo();
			window.location.reload();
      }
    }
  });
}
function saveteacherinfo(){
  var title  = $("#title").val();
  var firstname = $("#firstname").val();
  var middlename = $("#middlename").val();
  var lastname = $("#lastname").val();
	var genders = document.getElementsByName('gender');
var gender;
for(var i = 0; i < genders.length; i++){
    if(genders[i].checked){
        gender = genders[i].value;
    }
}
  var mobileno = $("#mobileno").val();
  var email = $("#email").val();
  var dateofbirth = $("#dateofbirth").val();
  var acategory = $("#acategory").val();
  var nationality = $("#nationality").val();
  var grno = $("#grno").val();
  var addmissionyear = $("#addmissionyear").val();
  var academicyear = $("#academicyear").val();
  var course = $("#course").val();
  var section = $("#section").val();
  var dateofenroll = $("#dateofenroll").val();
  // var password = $("#password").val();
  var msg ="";
	$("#msg").empty();
  if(title==""||firstname==""||lastname==""||gender==""||mobileno=="" ||email==""||dateofbirth==""||acategory==""||nationality==""||addmissionyear==""||course==""||section==""||dateofenroll==""){

    msg = '<div class="alert alert-danger">';
    msg += '    <button type="button" class="close" data-dismiss="alert">&times;</button>';
    msg += '    <strong>Wrong!</strong> Please Fill All Required Fields';
    msg += '</div>';
    $("#msg").append(msg);
  }
  else{

    $.ajax({
      type:'post',
      url:'insertteacherinformation.php',
      data:({
        title:title,
        firstname:firstname,
        middlename:middlename,
        lastname:lastname,
        gender:gender,
        mobileno:mobileno,
        email:email,
        dateofbirth:dateofbirth,
        acategory:acategory,
        nationality:nationality,
        grno:grno,
        addmissionyear:addmissionyear,
        academicyear:academicyear,
        course:course,
        section:section,
        dateofenroll:dateofenroll

      }),
      success:function(msg){
        msg = '<div class="alert alert-success">';
        msg += '    <button type="button" class="close" data-dismiss="alert">&times;</button>';
        msg += '        <strong>Well done!</strong> You successfully read this important alert message.';
        msg += '</div>';
        $("#msg").append(msg);
        $("#title").val("");
        $("#firstname").val("");
        $("#middlename").val("");
        $("#lastname").val("");
        $("#gender").val("");
        $("#mobileno").val("");
        $("#email").val("");
        $("#dateofbirth").val("");
        $("#acategory").val("");
        $("#nationality").val("");
        $("#grno").val("");
        $("#addmissionyear").val("");
        $("#academicyear").val("");
        $("#course").val("");
        $("#section").val("");
        $("#dateofenroll").val("");
        // $("#password").val("");
				// getteacherinfo();
				window.location.reload();
      }
    });
  }

}
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
