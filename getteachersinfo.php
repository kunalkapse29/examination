<?php
include 'connection.php';
session_start();
$uid = $_SESSION['userid'];
$response = [];
$sql = "SELECT id, userid, title, firstname, middlename, lastname, gender, mobileno, email, dateofbirth, acategory, nationality, grno, addmissionyear, academicyear, course, section, dateofenroll FROM teacherregistra ";
$result = mysqli_query($con,$sql);
if(mysqli_num_rows($result) > 0)
{
  while($row=mysqli_fetch_array($result))
  {
    array_push($response,[
      'id' => $row['id'],
      'userid' => $row['userid'],
      'title' => $row['title'],
      'firstname' => ucwords($row['firstname']),
      'middlename' => $row['middlename'],
      'lastname' => ucwords($row['lastname']),
      'gender' => $row['gender'],
      'mobileno' => $row['mobileno'],
      'email' => $row['email'],
      'dateofbirth' => $row['dateofbirth'],
      'acategory' => $row['acategory'],
      'nationality' => $row['nationality'],
      'grno' => $row['grno'],
      'addmissionyear' => $row['addmissionyear'],
      'academicyear' => $row['academicyear'],
      'course' => $row['course'],
      'section' => $row['section'],
      'dateofenroll' => $row['dateofenroll']
  ]);

  }
exit(json_encode($response));
}

?>
