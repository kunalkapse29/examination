<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="select2/datatablebootstrap4.css">
	  <link rel="shortcut icon" href="img/favicon.ico">
		<link href="select2/select2-bootstrap.min.css" rel="stylesheet" />
		<link href="select2/select4.css" rel="stylesheet" />
		<script src="select2/select4.js" type="text/javascript"></script>

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

      <?php include 'adminheader.php'; ?>
        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i> ADD SECTION</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content buttons">
              <div class="row">
              <div class="form-group has-success col-md-12">
                  <label class="control-label" for="inputWarning1">Section Name<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="text" class="form-control" id="sectionname"/>

              </div>
              </div>
							<div class="row">
              <div class="form-group has-success col-md-12">
                  	<label class="control-label" for="inputWarning1">Main Category Name</label>
									<!-- <span  id="sectionhtml"></span> -->
									<select name="maincatname" class="form-control select2-select" style="width:100%;" id="maincatname" required>
										<option value=""></option>
									</select>
              </div>
              </div>
              <div class="row">
              <div class="form-group has-success col-md-12">
                  <button class="btn btn-success" onclick="savesection()">Submit</button>

              </div>
              </div>

            </div>
        </div>
    </div>

</div>

<div class="row">
	<div class="box col-md-12">
			<div class="box-inner">
					<div class="box-header well" data-original-title="">
							<h2><i class="glyphicon glyphicon-list"></i> SECTION TABLE</h2>

							<div class="box-icon">
									<a href="#" class="btn btn-setting btn-round btn-default"><i
													class="glyphicon glyphicon-cog"></i></a>
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
													class="glyphicon glyphicon-chevron-up"></i></a>
									<a href="#" class="btn btn-close btn-round btn-default"><i
													class="glyphicon glyphicon-remove"></i></a>
							</div>
					</div>
					<div class="box-content buttons" style="overflow:auto;">
						<table class="table table-striped table-bordered " id="sectiontbl">
						<thead>
						<tr>
								<th style="text-align: center;">No</th>
								<th style="text-align: center;">Section Name</th>

								<th style="text-align: center;">Actions</th>
						</tr>
						</thead>
						<tbody id="sectionfetchcellvalue">


						</tbody>
						</table>

					</div>
			</div>
	</div>


</div><!--/row-->






    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>


		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<!-- <script src='js/jquery.dataTables.min.js'></script> -->
<script src="js/jquerydatatablemin.js"></script>
<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<link rel="stylesheet" href="assets/stylesheets/datatables/CSS/buttons.bootstrap4.min.css" />
<link rel="stylesheet" href="assets/stylesheets/datatables/CSS/dataTables.bootstrap4.min.css" />

<link rel="stylesheet" href="assets/stylesheets/datatables/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="assets/stylesheets/datatables/css/buttons.dataTables.min.css">
<script src="assets/stylesheets/datatables/jquery.dataTables.min.js"></script>
<script src="assets/stylesheets/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/stylesheets/datatables/dataTables.buttons.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/stylesheets/datatables/jszip.min.js"></script>
<script src="assets/stylesheets/datatables/pdfmake.min.js"></script>
<script src="assets/stylesheets/datatables/vfs_fonts.js"></script>
<script src="assets/stylesheets/datatables/buttons.html5.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.print.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.colVis.min.js"></script>
<script>
function getsectionname(){
    $("#sectionfetchcellvalue").empty();
     $.ajax({
          type:'POST',
          url:'getsectionname.php',
          success:function(msg){
                  var html ='';
                  var response = JSON.parse(msg);
                  var count = Object.keys(response).length;
                  for(var i=0;i<count;i++){
                    html +='<tr>';
                    html +='<td style="text-align: center;">'+(i+1)+'</td>';
                    html +='<td style="text-align: center;">'+response[i]['sectionname']+'</td>';
                    html +='<td style="text-align: center;">';
                    html +='<button class="btn btn-danger" onclick="removesection('+response[i]['id']+')">';
                    html +='<i class="glyphicon glyphicon-trash icon-white"></i></button>';
                    html +='</td></tr>';
                  }
                    $("#sectionfetchcellvalue").append(html);
										// $("#sectiontbl").dataTable();
										var table = $('#sectiontbl').DataTable( {
								lengthChange: false,
								buttons: [ 'copy', 'excel', 'csv', 'pdf']
						} );
						table.buttons().container()
						.appendTo( '#sectiontbl_wrapper .col-md-6:eq(0)' );
          }

     });
}
function setsectionname(){
  // $("#section").empty();
	$("#maincatname").empty();
  $.ajax({
       type:'POST',
       url:'getsectionname.php',
       success:function(msg){
               var html ='';
               var response = JSON.parse(msg);
               var count = Object.keys(response).length;
               html='<option value=""></option>';
               for(var i=0;i<count;i++){
                  html +='<option value="'+response[i]['id']+'">'+response[i]['sectionname']+'</option>';
               }


							  $("#maincatname").html(html);
              // $("#section").html(html);
       }

  });
}


$(document).ready(function(){
	setsectionname();
  getsectionname();
	$('select').select2({
			allowClear: true,
			placeholder: "Select here",
	});
});
function savesection(){
  var sectionname = $("#sectionname").val();
	var maincatname = $("#maincatname").val();

  if(sectionname==""){

  }
  else {
    $.ajax({
      type:'POST',
      url:'savesection.php',
      data:({sectionname:sectionname,maincatname:maincatname}),
      success:function(msg){

        var response = JSON.parse(msg);
      var count = Object.keys(response).length;

        if(count>0){
          alert("Section Added SuccessFully");
          window.location.reload();

        }
        else{
            alert("Already Exists");
        }
      }
    });
  }
}
function removesection(param){
  $.ajax({
    type:'POST',
    url:'removesectionbyid.php',
    data:({sectionid:param}),
    success:function(msg){
      var response = JSON.parse(msg);
      if(response){
        alert("Section Name removed SuccessFully");
        window.location.reload();
      }
    }
  });
}
function removeid(param){
  $.ajax({
    type:'POST',
    url:'removequestionbyid.php',
    data:({questionid:param}),
    success:function(msg){
      // alert(msg);
      var response = JSON.parse(msg);
      if(response){
        alert("Question removed SuccessFully");
        window.location.reload();
      }
    }
  });
}
function searchques(){
  var section = $("#section").val();
  $("#fetchcellvalue").empty();
  $.ajax({
    type:'POST',
    url:'searchquestionbysection.php',
    data:({section:section}),
    success:function(msg){
      var html ='';
        var response = JSON.parse(msg);
        var count = Object.keys(response).length;
        for(var i=0;i<count;i++){

        html +='<tr>';
        html +='<td style="text-align: center;">'+(i+1)+'</td>';
        html +='<td style="text-align: center;">'+response[i]['questionno']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['question']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['option1']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['option2']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['option3']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['option4']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['correctoption']+'</td>';
        html +='<td style="text-align: center;">';
        html +='<button class="btn btn-danger" onclick="removeid('+response[i]['questionno']+')">';
        html +='<i class="glyphicon glyphicon-trash icon-white"></i></button>';
        html +='      </td>';
        html +='</tr>';
        }
        $("#fetchcellvalue").append(html);
        // $("#question").dataTable();
				var table = $('#question').DataTable( {
		lengthChange: false,
		buttons: [ 'copy', 'excel', 'csv', 'pdf']
} );
table.buttons().container()
.appendTo( '#question_wrapper .col-md-6:eq(0)' );
    }
  });
}
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
