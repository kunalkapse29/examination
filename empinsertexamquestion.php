<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>

    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>


            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <!-- left menu starts -->
        <?php include 'employeeheader.php'; ?>
        <!--/span-->
        <!-- left menu ends -->


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
                <div>
        <ul class="breadcrumb">
            <li>
                <a href="#">Home</a>
            </li>
            <li>
                <a href="#">Exam Module</a>
            </li>
        </ul>
    </div>

    <div class="row">
        <div class="box col-md-12">
            <div class="box-inner">
                <div class="box-header well" data-original-title="">
                    <h2><i class="glyphicon glyphicon-th"></i> INSERT EXAM QUESTION</h2>

                    <div class="box-icon">
                        <a href="#" class="btn btn-setting btn-round btn-default"><i
                                class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                class="glyphicon glyphicon-chevron-up"></i></a>
                        <a href="#" class="btn btn-close btn-round btn-default"><i
                                class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-6">

                            <label class="control-label" for="inputWarning1">Section <sup>
                              <font color="red" size="3px;">*</font></sup></label>
                          <select  class="control-label" id="section" data-rel="chosen">
                          <option value="">---Select Section---</option>
                          <option value="html">html</option>
                          <option value="php">php</option>
                          <option value="js">javascript</option>
                          </select>
                        </div>

                        <div class="col-md-6">

                            <label class="control-label" for="inputWarning1">Correct Option <sup>
                              <font color="red" size="3px;">*</font></sup></label>
                          <select class="control-label" id="correctoption" data-rel="chosen">
                          <option value="">---Select Option---</option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                          <div class="form-group">
                          <strong>Option 1</strong>
                          <input type="text" id="option1" style="width: 100%;"></input>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <strong>Option 2</strong>
                            <input type="text" id="option2" style="width: 100%;"></input>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <strong>Option 3</strong>
                            <input type="text" id="option3" style="width: 100%;"></input>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="form-group">
                            <strong>Option 4</strong>
                            <input type="text" id="option4" style="width: 100%;"></input>
                          </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <strong>Question</strong>
                          <textarea rows="4" id="question" style="width:100%;"></textarea>
                      </div>
                    </div><br/>
                    <div class="row">
                      <div class="col-md-12">
                      <div class="form-group has-success col-md-4">
                          <button type="button" class="btn btn-primary" onclick="savequestionanswer()">Save</button>
                      </div>
                      <div class="form-group has-success col-md-8">
                      </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!--/span-->
    </div><!--/row-->

   <div class="row">
    <div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well">
                <h2>UPDATE  QUESTION</h2>

                <div class="box-icon">
                  <a href="#" class="btn btn-setting btn-round btn-default"><i
                          class="glyphicon glyphicon-cog"></i></a>
                  <a href="#" class="btn btn-minimize btn-round btn-default"><i
                          class="glyphicon glyphicon-chevron-up"></i></a>
                  <a href="#" class="btn btn-close btn-round btn-default"><i
                          class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
              <div class="row">
                <div class="col-md-12">
                <div class="form-group has-success col-md-4">
                  <button type="button" class="btn btn-primary" onclick="opentextfield()">
                  Open Existing Question
                  </button>
                </div>
                <div class="form-group has-success col-md-8">
                  <span id="enterquestionno"></span>
                <input type="text" id="questionid" name="questionid" style="display:none;" onchange="questionno(this.value)"/>
                </div>

              </div>
              </div>
            </div>
        </div>
    </div>
  </div>
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->



    <hr>



		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script type="text/javascript">

    function clearfunction(){
      var form = document.getElementById("questionmodule");
      form.reset();
    }
    function funclear(param){
      $('.messageCheckbox'+param).prop("checked", false);
    }
    function savequestionanswer(){
              var section = $("#section").val();
              var question = $("#question").val();
              var correctoption = $("#correctoption").val();
              var option1 = $("#option1").val();
              var option2 = $("#option2").val();
              var option3 = $("#option3").val();
              var option4 = $("#option4").val();
              if(section==""|| question==""||correctoption==""||option1==""||option2==""||option3==""||option4==""){
                 alert("Please Select All Fields");
              }
              else{
               $.ajax({
                    url: "savequestionanswer.php",
                    type: "POST",
                    data: ({
                       section: section,
                       question: question,
                       correctoption: correctoption,
                       option1:option1,
                       option2:option2,
                       option3:option3,
                       option4:option4

                    }),
                    success: function(data) {
                        alert("Question and Answer Save successfully");
                        $("#question").val("");
                        $("#option1").val("");
                        $("#option2").val("");
                        $("#option3").val("");
                        $("#option4").val("");
                       },
                        error: function(data, errorThrown) {
                        }
                    });
                  }
           }
 function opentextfield(){

     $("#enterquestionno").html("<strong>Enter question No</strong>");
     document.getElementById("questionid").style.display="block";
 }
 function questionno(param){

      $.ajax({
           url: "getquestionanswer.php",
           type: "GET",
           data: ({
              questionno: param
           }),
           success: function(data) {
                response = JSON.parse(data);
                $("#question").val(response['question']);
                $("#option1").val(response['option1']);
                $("#option2").val(response['option2']);
                $("#option3").val(response['option3']);
                $("#option4").val(response['option4']);

              },
               error: function(data, errorThrown) {
               }
           });
 }
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
