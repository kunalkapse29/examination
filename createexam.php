    <?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
  	<link href='css/editor.css' rel='stylesheet'>
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css"> -->
	<link rel="stylesheet" href="select2/datatablebootstrap4.css">
	<!-- <link rel="stylesheet" href="select2/dtable.css"> -->
<!-- <link rel="stylesheet" href="select2/dtable1.css"> -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

		<link rel="stylesheet" href="css/fontawesome.css">
		<link href="select2/select2-bootstrap.min.css" rel="stylesheet" />
		<link href="select2/select4.css" rel="stylesheet" />
	<script src="select2/select4.js" type="text/javascript"></script>
</head>

<body>
	 <!-- instruction and notice modal -->
	<div id="instructionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Instruction / Notice</h4>
				<input type="hidden" id="instructionid" />
      </div>
      <div class="modal-body">
				<div class="row">
					<div class="col-lg-12 nopadding">
						<textarea id="txteditor"></textarea>
						<span id="selecttexteditor"></span>
					</div>
				</div>
      </div>
      <div class="modal-footer">
				<button type="button" class="btn btn-success" onclick="saveinstruction()">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
 </div>
 <!--instruction and notice modal close  -->
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <?php include 'adminheader.php'; ?>


        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Create Exam</a>
        </li>
    </ul>
</div>
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i> Create Exam</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content buttons">
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Exam Title<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="text" class="form-control" id="examtitle"></input>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Exam Desc<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="text" class="form-control"  id="examdesc"></input>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Section Name<sup><font color="red" size="3px;">*</font></sup></label>
                  <!-- <span  id="sectionhtml"></span> -->
									<select name="sectionhtml" class="form-control select2-select" style="width:100%;" id="sectionhtml" required>
										<option value=""></option>
									</select>
              </div>

              </div>
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Start Date<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="date"  class="form-control" id="startdate" style="width:100%;padding-right: 3px;"></input>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label" >End Date<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="date"  class="form-control" id="enddate" style="width:100%;padding-right: 3px;"></input>
              </div>
							<div class="form-group col-md-4">
									<label class="control-label" >Total Time<sup><font color="red" size="3px;">*</font></sup></label>
									<input type="time"  class="form-control" id="totaltime" style="width:100%;padding-right: 3px;" step="1"></input>
							</div>
              </div>
              <div class="row">
              <div class="form-group col-md-4">
                    <label class="control-label" >Total Question<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="number"  class="form-control" id="totalquestion" style="width:100%;padding-right: 3px;"></input>
              </div>

              <div class="form-group col-md-4">
                  <label class="control-label" >Total Marks<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="number"  class="form-control" id="totalmarks" style="width:100%;padding-right: 3px;" ></input>
              </div>
              <div class="form-group col-md-4">
                  <label class="control-label" >Mark / Question<sup><font color="red" size="3px;">*</font></sup></label>
                  <input type="number"  class="form-control" id="perquestionmark" style="width:100%;padding-right: 3px;"> </input>
              </div>
              </div>

              <div class="row">
              <div class="form-group has-success col-md-4">
                  <button class="btn btn-primary" onclick="savecreateexam()">Submit</button>

              </div>
              </div>

            </div>
        </div>
    </div>

</div>

<div class="row">
<div class="box col-md-12">
<div class="box-inner">
<div class="box-header well" data-original-title="">
    <h2><i class="glyphicon glyphicon-user"></i>Exam Schedule</h2>

    <div class="box-icon">
        <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
        <a href="#" class="btn btn-minimize btn-round btn-default"><i
                class="glyphicon glyphicon-chevron-up"></i></a>
        <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
    </div>
</div>
<div class="box-content" style="overflow:auto;">


<table class="table table-striped table-bordered" id="notice">
<thead>
<tr>
    <th style="text-align: center;">No</th>
    <th style="text-align: center;">Exam Title</th>
    <th style="text-align: center;">Section</th>
    <th style="text-align: center;">Date</th>
    <th style="text-align: center;">Status</th>
    <th style="text-align: center;">Actions</th>
</tr>
</thead>
<tbody id="fetchcellvalue">


</tbody>
</table>
</div>
</div>
</div>
<!--/span-->

</div><!--/row-->

    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>

		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<script src="js/jquerydatatablemin.js"></script>

<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script src="js/editor.js"></script>
<link rel="stylesheet" href="assets/stylesheets/datatables/CSS/buttons.bootstrap4.min.css" />
<link rel="stylesheet" href="assets/stylesheets/datatables/CSS/dataTables.bootstrap4.min.css" />

<link rel="stylesheet" href="assets/stylesheets/datatables/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="assets/stylesheets/datatables/css/buttons.dataTables.min.css">
<script src="assets/stylesheets/datatables/jquery.dataTables.min.js"></script>
<script src="assets/stylesheets/datatables/dataTables.bootstrap4.min.js"></script>
<script src="assets/stylesheets/datatables/dataTables.buttons.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.bootstrap4.min.js"></script>
<script src="assets/stylesheets/datatables/jszip.min.js"></script>
<script src="assets/stylesheets/datatables/pdfmake.min.js"></script>
<script src="assets/stylesheets/datatables/vfs_fonts.js"></script>
<script src="assets/stylesheets/datatables/buttons.html5.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.print.min.js"></script>
<script src="assets/stylesheets/datatables/buttons.colVis.min.js"></script>
<script>

function getsectionname(){
   var html ='';
  $("#sectionhtml").empty();
  $.ajax({
       type:'POST',
       url:'getsectionname.php',
       success:function(msg){

               var response = JSON.parse(msg);
               var count = Object.keys(response).length;
               html='<select  class="control-label" id="section" data-rel="chosen" style="width:100%;"><option value="">Select Here</option>';
               for(var i=0;i<count;i++){
                  html +='<option value="'+response[i]['sectionname']+'">'+response[i]['sectionname']+'</option>';
               }
               html+='</select>';

              $("#sectionhtml").html(html);
       }

  });
}
// setsectionname();
function saveinstruction(){
	var id = $("#instructionid").val();
	var txteditor = $("#txteditor").Editor("getText");
	if(txteditor==""){
   $("#selecttexteditor").html("<font color='red'>Select TextEditor Value</font>");
	}
	else{
		$("#selecttexteditor").html("");
		$('#instructionModal').modal('hide');
	$.ajax({
		type:'POST',
		url:'saveinstructionnotice.php',
		data:({
			id:id,
			txteditor:txteditor
		}),
	  success:function(msg){
       window.location.reload();
       // $("#instructionModal .close").modal();
		}
	});
}

}
$(document).ready(function(){
  getsectionname();
   searchques();
	 $("#txteditor").Editor();


							 $('select').select2({
									 allowClear: true,
									 placeholder: "Select here",
							 });

});
function savecreateexam(){
  var examtitle = $("#examtitle").val();
  var examdesc = $("#examdesc").val();
  var startdate = $("#startdate").val();
  var enddate = $("#enddate").val();
  var totaltime = $("#totaltime").val();
  // alert(totaltime);
  var arr = totaltime.split(":");
  // alert(arr[0]);
  var hour = arr[0];
  var minute =  arr[1];
  var second = arr[2];
  var sectionname = $("#sectionhtml").val();
  var totalquestion = $("#totalquestion").val();
  var totalmarks = $("#totalmarks").val();
  var perquestionmark = $("#perquestionmark").val();
  if(sectionname=="" || examtitle=="" || examdesc==""|| startdate=="" || enddate=="" || totaltime==""  || totalquestion=="" || totalmarks==""  ){
     alert("All Fields are Required");
  }
  else {
    $.ajax({
      type:'POST',
      url:'savecreateexam.php',
      data:({
        examtitle:examtitle,
        examdesc:examdesc,
        startdate:startdate,
        enddate:enddate,
        hour:hour,
        minute:minute,
        second:second,
        sectionname:sectionname,
        totalquestion:totalquestion,
        totalmarks:totalmarks,
        perquestionmark:perquestionmark

      }),
      success:function(msg){
      var response = JSON.parse(msg);
      if(response){
         alert("Exam Created");
         $("#examtitle").val("");
         $("#examdesc").val("");
         $("#startdate").val("");
         $("#enddate").val("");
         $("#totaltime").val("");
         $("#sectionhtml").val("");
         $("#totalquestion").val("");
         $("#totalmarks").val("");
         $("#perquestionmark").val("");
				 searchques();
         window.location.reload();
      }
      }
    });
  }
}
function removesection(param){
  $.ajax({
    type:'POST',
    url:'removesectionbyid.php',
    data:({sectionid:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        alert("Section Name removed SuccessFully");
        window.location.reload();
      }
    }
  });
}
function removeid(param){

  $.ajax({
    type:'POST',
    url:'removecreateexambyid.php',
    data:({createexam:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        alert("Create Exam removed SuccessFully");
      // searchques();
      window.location.reload();
      }
    }
  });
}
function active(param){
	$.ajax({
    type:'POST',
    url:'activeexambyid.php',
    data:({createexamid:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        // searchques();
        window.location.reload();
      }
    }
  });
}
function inactive(param){
	$.ajax({
    type:'POST',
    url:'inactiveexambyid.php',
    data:({createexamid:param}),
    success:function(msg){

      var response = JSON.parse(msg);
      if(response){
        // searchques();
        window.location.reload();
      }
    }
  });
}
function openmodal(param){
	$("#instructionid").val(param);
}
function searchques(){
  $("#fetchcellvalue").empty();
  $.ajax({
    type:'POST',
    url:'searchcreateexam.php',
    success:function(msg){
      var html ='';
        var response = JSON.parse(msg);
        var count = Object.keys(response).length;
        for(var i=0;i<count;i++){

        html +='<tr>';
        html +='<td style="text-align: center;">'+(i+1)+'</td>';
        html +='<td style="text-align: center;"><a href="infocreateexam.php?id='+response[i]['id']+'">'+response[i]['examtitle'].toUpperCase()+'</a></td>';
        // html +='<td style="text-align: center;">'+response[i]['examdesc']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['sectionname']+'</td>';
        html +='<td style="text-align: center;">'+response[i]['startdate']+' to '+response[i]['enddate']+'</td>';
      	if(response[i]['flag']==0){
				html +='<td style="text-align: center;"><button type="button" class="btn btn-link" title="For Inactive" onclick="active('+response[i]['id']+')"><span class="label-success label label-default">Active</span></button></td>';
				}
        else {
        html +='<td style="text-align: center;"><button type="button" class="btn btn-link" title="For Active" onclick="inactive('+response[i]['id']+')"><span class="label-default label">Inactive</span></button></td>';
        }
        html +='<td style="text-align: center;">';
				html +='<button type="button" class="btn btn-success" data-toggle="modal" title="Add Instructions" data-target="#instructionModal" onclick="openmodal('+response[i]['id']+')"><i class="glyphicon glyphicon-plus"></i></button>';
				html +='<a href="editcreateexam.php?id='+response[i]['id']+'"><button type="button" class="btn btn-primary" title="For Edit"  ><i class="glyphicon glyphicon-edit"></i></button></a>';
				html +='<button class="btn btn-danger" title="For Remove" onclick="removeid('+response[i]['id']+')">';
				html +='<i class="glyphicon glyphicon-trash icon-white"></i></button>';
				html +='</td>';

        html +='</tr>';
        }
        $("#fetchcellvalue").append(html);
        // $("#notice").dataTable();
        var table = $('#notice').DataTable( {
    lengthChange: false,
    buttons: [ 'copy', 'excel', 'csv', 'pdf']
} );
table.buttons().container()
.appendTo( '#notice_wrapper .col-md-6:eq(0)' );

    }
  });
}
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
