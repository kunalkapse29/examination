<div class="col-sm-2 col-lg-2">
    <div class="sidebar-nav">
        <div class="nav-canvas">
            <div class="nav-sm nav nav-stacked">

            </div>
            <ul class="nav nav-pills nav-stacked main-menu">
                <li class="nav-header">Main</li>
                <li><a class="ajax-link" href="employeeindex.php"><i class="glyphicon glyphicon-home"></i><span>Employee Dashboard</span></a>
                </li>
                <li class="accordion">
                    <a href="#"><i class="glyphicon glyphicon-plus"></i><span>Examination Section</span></a>
                    <ul class="nav nav-pills nav-stacked">
                      <li><a class="ajax-link" href="empcreateexam.php"><i
                                  class="glyphicon glyphicon-th"></i><span>Create Exam</span></a></li>
                      <li><a class="ajax-link" href="empsearchexamquestion.php"><i
                                  class="glyphicon glyphicon-th"></i><span>Search Questions</span></a></li>
                      <li><a class="ajax-link" href="empinsertexamquestion.php"><i
                                  class="glyphicon glyphicon-th"></i><span>Question Set</span></a></li>
                    </ul>
                </li>
                <li><a href="empstudent.php"><i class="glyphicon glyphicon-lock"></i><span> Student Registration</span></a>
                </li>

            </ul>

        </div>
    </div>
</div>
