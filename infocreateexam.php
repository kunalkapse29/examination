<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
$user_id = $_SESSION['userid'];
$user_name =  $_SESSION['username'];
$id = $_REQUEST['id'];

}
if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
    <link href='css/editor.css' rel='stylesheet'>
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="stylesheet" href="css/fontawesome.css">
    <link href="select2/select2-bootstrap.min.css" rel="stylesheet" />
    <link href="select2/select4.css" rel="stylesheet" />
  <script src="select2/select4.js" type="text/javascript"></script>
  <style media="screen">

.easyPaginateNav a, .easyPaginateNav strong {
    background: #fff;
    display: inline-block;
    margin-right: 3px;
    padding: 4px 12px;
    text-decoration: none;
  line-height: 1.5em;

    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
}
.easyPaginateNav a:hover {
    background-color: #BEBEBE;
    color: #fff;
}
.easyPaginateNav a:active {
  background: rgba(190, 190, 190, 0.75);
}
.easyPaginateNav strong {
    color: #fff;
  }
  </style>
  <script src="jquery.easyPaginate.js"></script>
    <!-- <style>
    dl {
    border: 3px double #ccc;
    padding: 0.5em;
  }
  dt {
    float: left;
    clear: left;
    width: 100px;
    text-align: right;
    font-weight: bold;
    color: green;
  }
  dt::after {
    content: ":";
  }
  dd {
    margin: 0 0 0 110px;
    padding: 0 0 0.5em 0;
  }
    </style> -->

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

         <div class="navbar-inner">
              <button type="button" class="navbar-toggle pull-left animated flip">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <!-- user dropdown starts -->
              <div class="btn-group pull-right">
                  <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                      <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                      <li><a href="#">Profile</a></li>
                      <li class="divider"></li>
                      <li><a href="logout.php">Logout</a></li>
                  </ul>
              </div>
              <!-- user dropdown ends -->

              <!-- theme selector starts -->
              <div class="btn-group pull-right theme-container animated tada">
                  <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-tint"></i><span
                          class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                      <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" id="themes">
                      <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                      <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                      <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                      <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                      <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                      <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                      <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                      <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                      <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                  </ul>
              </div>
              <!-- theme selector ends -->



          </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
  <?php include 'adminheader.php'; ?>


  <div id="content" class="col-lg-10 col-sm-10">
<div class="row">


    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
              <h2  id="headerinfo">Description List</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <dl id="infodata">

                </dl>
                <div class="row">
                <div class="col-sm-4">
                <div class="form-group">
                  <a href="editcreateexam.php?id=<?php echo $id; ?>"><button  type="button" class="btn btn-success" title="Edit"  >Edit</button></a>
  <!-- <button class="btn btn-success"  title="Edit">Edit</button> -->
                </div>
                </div>
                <div class="col-sm-8">
                <div class="form-group">
                </div>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!--/span-->


</div><!--/row-->
<div class="row">
    <div class="box col-md-12">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-th"></i> QUESTION</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">


                <div class="row">
                  <div class="col-md-12" style="padding-top: 10px;padding-bottom: 10px;">

                  </div>
                </div>
                <!-- <div class="row"><div class="col-md-8"></div><b>checkbox</b></div> -->
                <div id="div1">

                </div>

             </div>
              <br/>

            </div>
        </div>
    </div>
  </div>
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->




    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>

    <footer class="row">
      <div class="text-center">
        <small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
      </div>
    </footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<!-- <script src='js/jquery.dataTables.min.js'></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- <script src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js'></script> -->
<script src="js/jquerydatatablemin.js"></script>
<!-- <script src='js/jquerydatatable.js'></script> -->
<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script src="js/editor.js"></script>
<script>

function getinformation(){
  var html = "";
  $.ajax({
       type:'POST',
       url:'getinformation.php',
       data:({idinfo:<?php echo $id;?>}),
       dataType:'json',
       success:function(response){
         $("#headerinfo").html(response['examtitle']);
           // html+="<dt><a>Exam Title</a></dt>";
           // html+="<dd>"+response['examtitle']+"</dd>";
           html+="<div class='row'><div class='col-sm-4'><div class='form-group'><a><b>Exam Description</b></a><br/>";
           html+=""+response['examdesc'].toUpperCase()+"</div></div>";
           html+="<div class='col-sm-4'><div class='form-group'><a><b>Section Name</b></a><br/>";
           html+=""+response['sectionname'].toUpperCase()+"</div></div>";
           if(response['flag']==0){
             html+="<div class='col-sm-4'><div class='form-group'><a><b>Status</b></a><br/>";
             html+="ACTIVE</div></div></div>";
           }
           else {
             html+="<div class='col-sm-4'><div class='form-group'><a><b>Status</b></a><br/>";
             html+="INACTIVE</div></div></div>";
           }

           html+="<div class='row'><div class='col-sm-4'><div class='form-group'><a><b>Start Date</b></a><br/>";
           html+=""+response['startdate'].toUpperCase()+"</div></div>";
           html+="<div class='col-sm-4'><div class='form-group'><a><b>End Date</b></a><br/>";
           html+=""+response['enddate'].toUpperCase()+"</div></div>";
           html+="<div class='col-sm-4'><div class='form-group'><a><b>Duration</b></a><br/>";
           html+=""+response['hour']+"Hr"+response['minute']+"Min"+response['second']+"Sec</div></div></div>";
           html+="<div class='row'><div class='col-sm-4'><div class='form-group'><a><b>Total Question</b></a><br/>";
           html+=""+response['totalquestion'].toUpperCase()+"</div></div>";
           html+="<div class='col-sm-4'><div class='form-group'><a><b>Total Marks</b></a><br/>";
           html+=""+response['totalmarks'].toUpperCase()+"</div></div>";
           html+="<div class='col-sm-4'><div class='form-group'><a><b>Marks / Question</b></a><br/>";
           html+=""+response['perquestionmark'].toUpperCase()+"Mark</div></div></div>";

           // html+="<dt>Status</dt>";
           // html+="<dd>"+response['flag']+"</dd>";
          $("#infodata").html(html);

       }

  });
}
function display_qnbank() {
   $.ajax({
          url: "displayquestionbyid.php",
          type: "GET",
           data:({idinfo:<?php echo $id;?>}),
          success: function(data) {
             // alert("ok");
              $("#div1").html("");
               $("#divnav").html("");
              var html1 ="";
           var response = JSON.parse(data);
           var count = Object.keys(response).length;
           for (var i = 0; i < count; i++) {
             var j=i+1;
               var chk1=chk2=chk3=chk4="";
               var used="";
               if(response[i]['correctoption']=='1')
               {
                    chk1='<strong class="green">Option 1 : '+response[i]['option1']+'</strong>';
               }
               else {
                 chk1='<strong>Option 1 : '+response[i]['option1']+'</strong>';
               }
               if(response[i]['correctoption']=='2')
               {
                    chk2='<strong class="green">Option 2 : '+response[i]['option2']+'</strong>';
               }
               else {
                 chk2='<strong>Option 2 : '+response[i]['option2']+'</strong>';
               }
               if(response[i]['correctoption']=='3')
               {
                    chk3='<strong class="green">Option 3 : '+response[i]['option3']+'</strong>';
               }
               else {
                 chk3='<strong>Option 3 : '+response[i]['option3']+'</strong>';
               }
               if(response[i]['correctoption']=='4')
               {
                    chk4='<strong class="green">Option 4 : '+response[i]['option4']+'</strong>';
               }
               else {
                 chk4='<strong>Option 4 : '+response[i]['option4']+'</strong>';
               }
               used=response[i]['used'];
               if(!(used))
               {
                   used="Unused Question";
               }


               $("#div1").append('<span><div class="panel panel-default"><div class="panel-heading" ><h4 class="red">Question '+(j)+'</h4></div><div class="panel-body"><div class="row"><div class="col-md-8"><a><b>'+response[i]['quetion']+
               '</a></b></div><div class="col-md-2"><input type="checkbox" id="mycheckbox" value="'+response[i]['srno']+
               '" required></div><div class="col-md-2"><a style="color:orange"  data-toggle="collapse" href="#'+j+'" data-target="#'+i+'">Used In</a><div id="'+i+'" class="collapse">'+used+
               '</div></div></div><a style="color:orange" href="#'+i+'" data-toggle="collapse" data-target="#'+i+'1"><b>Answers</b></a><div id="'+i+
               '1" class="collapse"><div class="row"><div class="col-sm-1"></div><div class="col-md-10">'+chk1+'</div></div><div class="row"><div class="col-sm-1"></div><div class="col-md-10">'+chk2+'</div></div><div class="row"><div class="col-sm-1"></div><div class="col-md-10">'+chk3+'</div></div><div class="row"><div class="col-sm-1"></div><div class="col-md-10">'+chk4+'</div></div></div></div></div></span>');

           }
            // $("#div1").html(html1);
            applypagination(count);


             },
              error: function(data, errorThrown) {
              }
          });
}
function applypagination(param){
  if(param>0){

    $('#div1').easyPaginate({
       paginateElement: 'span',
       elementsPerPage: 5,
       effect: 'climb'
   });
  }
  else{

  }
}
$(document).ready(function(){
getinformation();
display_qnbank();
});
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
