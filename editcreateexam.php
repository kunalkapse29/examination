<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
$user_id = $_SESSION['userid'];
$user_name =  $_SESSION['username'];
$id = $_REQUEST['id'];

}
if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>
    <link href='css/editor.css' rel='stylesheet'>
    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">
    <link rel="stylesheet" href="css/fontawesome.css">
    <link href="select2/select2-bootstrap.min.css" rel="stylesheet" />
    <link href="select2/select4.css" rel="stylesheet" />
  <script src="select2/select4.js" type="text/javascript"></script>
    <!-- <style>
    dl {
    border: 3px double #ccc;
    padding: 0.5em;
  }
  dt {
    float: left;
    clear: left;
    width: 100px;
    text-align: right;
    font-weight: bold;
    color: green;
  }
  dt::after {
    content: ":";
  }
  dd {
    margin: 0 0 0 110px;
    padding: 0 0 0.5em 0;
  }
    </style> -->

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

         <div class="navbar-inner">
              <button type="button" class="navbar-toggle pull-left animated flip">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
              </button>
              <!-- user dropdown starts -->
              <div class="btn-group pull-right">
                  <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> admin</span>
                      <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                      <li><a href="#">Profile</a></li>
                      <li class="divider"></li>
                      <li><a href="logout.php">Logout</a></li>
                  </ul>
              </div>
              <!-- user dropdown ends -->

              <!-- theme selector starts -->
              <div class="btn-group pull-right theme-container animated tada">
                  <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                      <i class="glyphicon glyphicon-tint"></i><span
                          class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                      <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" id="themes">
                      <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                      <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                      <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                      <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                      <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                      <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                      <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                      <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                      <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                  </ul>
              </div>
              <!-- theme selector ends -->



          </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">
  <?php include 'adminheader.php'; ?>


    <div id="content" class="col-lg-10 col-sm-10">
  <div class="row">
      <div class="box col-md-12">
          <div class="box-inner">
              <div class="box-header well" data-original-title="">
                  <h2><i class="glyphicon glyphicon-list"></i> Edit Exam</h2>

                  <div class="box-icon">
                      <a href="#" class="btn btn-setting btn-round btn-default"><i
                              class="glyphicon glyphicon-cog"></i></a>
                      <a href="#" class="btn btn-minimize btn-round btn-default"><i
                              class="glyphicon glyphicon-chevron-up"></i></a>
                      <a href="#" class="btn btn-close btn-round btn-default"><i
                              class="glyphicon glyphicon-remove"></i></a>
                  </div>
              </div>
              <div class="box-content buttons">
                <div class="row">
                <div class="form-group col-md-4">
                      <label class="control-label" >Exam Title<sup><font color="red" size="3px;">*</font></sup></label>
                      <input type="text" class="form-control" id="examtitle1"></input>
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label" >Exam Desc<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="text" class="form-control"  id="examdesc1"></input>
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label" >Section Name<sup><font color="red" size="3px;">*</font></sup></label>
                   <input type="hidden"  id="sectionhidden"></input>
  									<select name="sectionhtml" class="form-control select2-select" style="width:100%;" id="sectionhtml1" required>
  										<option value=""></option>
  									</select>
                </div>

                </div>
                <div class="row">
                <div class="form-group col-md-4">
                      <label class="control-label" >Start Date<sup><font color="red" size="3px;">*</font></sup></label>
                      <input type="date" class="form-control" id="startdate1" style="width:100%;padding-right: 3px;"></input>
                </div>

                <div class="form-group col-md-4">
                    <label class="control-label" >End Date<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="date" class="form-control" id="enddate1" style="width:100%;padding-right: 3px;"></input>
                </div>
  							<div class="form-group col-md-4">
  									<label class="control-label" >Total Time<sup><font color="red" size="3px;">*</font></sup></label>
  									<input type="time"  class="form-control" id="totaltime1" style="width:100%;padding-right: 3px;" step="1"></input>
  							</div>
                </div>
                <div class="row">
                <div class="form-group col-md-4">
                      <label class="control-label" >Total Question<sup><font color="red" size="3px;">*</font></sup></label>
                      <input type="number" class="form-control" id="totalquestion1" style="width:100%;padding-right: 3px;"></input>
                </div>

                <div class="form-group col-md-4">
                    <label class="control-label" >Total Marks<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="number"  class="form-control" id="totalmarks1" style="width:100%;padding-right: 3px;"></input>
                </div>
                <div class="form-group col-md-4">
                    <label class="control-label" >Mark / Question<sup><font color="red" size="3px;">*</font></sup></label>
                    <input type="number" class="form-control" id="perquestionmark1" style="width:100%;padding-right: 3px;"></input>
                </div>
                </div>

                <div class="row">
                <div class="form-group has-success col-md-4">
                    <button class="btn btn-primary" onclick="savecreateexam()">Submit</button>
                    <a href="createexam.php"><button class="btn btn-success" >Back</button></a>
                </div>
                </div>

              </div>
          </div>
      </div>

  </div>
</div>
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->




    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" data-dismiss="modal">Save changes</a>
                </div>
            </div>
        </div>
    </div>

    <footer class="row">
      <div class="text-center">
        <small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
      </div>
    </footer>

</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<!-- <script src='js/jquery.dataTables.min.js'></script> -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<!-- <script src='https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js'></script> -->
<script src="js/jquerydatatablemin.js"></script>
<!-- <script src='js/jquerydatatable.js'></script> -->
<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script src="js/editor.js"></script>
<script>

function getsectionname(){
   var html ='';
  // $("#sectionhtml1").empty();
  $.ajax({
       type:'POST',
       url:'getsectionname.php',
       success:function(msg){

               var response = JSON.parse(msg);
               var count = Object.keys(response).length;
               html='<select  class="control-label" id="section" data-rel="chosen" style="width:100%;"><option value="">Select Here</option>';
               for(var i=0;i<count;i++){
                  html +='<option value="'+response[i]['sectionname']+'">'+response[i]['sectionname']+'</option>';
               }
               html+='</select>';

              $("#sectionhtml1").html(html);
       }

  });
}

function getinformation(){

  $.ajax({
       type:'POST',
       url:'geteditinformation.php',
       data:({idinfo:<?php echo $id;?>}),
       dataType:'json',
       success:function(response){
         $("#examtitle1").val(response['examtitle']);
         $("#examdesc1").val(response['examdesc']);
         $("#sectionhidden").val(response['sectionname']);
         // $("#sectionhtml1").select2().val(response['sectionname']).trigger('change.select2');
         $("#sectionhtml1").html('<option value="'+response['sectionname']+'">'+response['sectionname']+'</option>');
         $("#startdate1").val(response['startdate']);
         $("#enddate1").val(response['enddate']);

         document.getElementById("totaltime1").value = response['totaltime'];
         // $("#totaltime1").append(response['totaltime']);
         $("#totalquestion1").val(response['totalquestion']);
         $("#totalmarks1").val(response['totalmarks']);
         $("#perquestionmark1").val(response['perquestionmark']);
       }

  });
}
function savecreateexam(){
  var examtitle = $("#examtitle1").val();
  var examdesc = $("#examdesc1").val();
  var startdate = $("#startdate1").val();
  var enddate = $("#enddate1").val();
  var totaltime = $("#totaltime1").val();
  var arr = totaltime.split(":");
  // alert(arr[0]);
  var hour = arr[0];
  var minute =  arr[1];
  var second = arr[2];
  var sectionname = $("#sectionhtml1").val();

  if(sectionname===""){
      sectionname = $("#sectionhidden").val();
  }
  var totalquestion = $("#totalquestion1").val();
  var totalmarks = $("#totalmarks1").val();
  var perquestionmark = $("#perquestionmark1").val();
  if(sectionname=="" || examtitle=="" || examdesc==""|| startdate=="" || enddate=="" || totaltime==""  || totalquestion=="" || totalmarks==""  ){
     alert("All Fields are Required");
  }
  else {
    $.ajax({
      type:'POST',
      url:'updatecreateexam.php',
      data:({
        idinfo:<?php echo $id;?>,
        examtitle:examtitle,
        examdesc:examdesc,
        startdate:startdate,
        enddate:enddate,
        hour:hour,
        minute:minute,
        second:second,
        sectionname:sectionname,
        totalquestion:totalquestion,
        totalmarks:totalmarks,
        perquestionmark:perquestionmark

      }),
      success:function(msg){
      var response = JSON.parse(msg);
      if(response){
         alert("Exam Updated");
				 window.location.href="createexam.php";
      }
      }
    });
  }
}
$(document).ready(function(){

getinformation();
getsectionname();
$('select').select2({
    allowClear: true,
    placeholder: "Select here",
});
});
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
