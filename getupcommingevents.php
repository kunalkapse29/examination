<?php
include 'connection.php';
session_start();
$response = [];
$date = date("Y-m-d");

$sql = "SELECT examtitle,examdesc,startdate,enddate,hour,minute,second,totalquestion,totalmarks FROM createexam WHERE startdate >='$date'  and flag=1";
$result = mysqli_query($con,$sql);
if(mysqli_num_rows($result) > 0)
{
  while($row=mysqli_fetch_array($result))
  {
    array_push($response,[
      'examtitle' => $row['examtitle'],
      'examdesc' => $row['examdesc'],
      'startdate' => $row['startdate'],
      'enddate' => $row['enddate'],
      'hour' => $row['hour'],
      'minute' => $row['minute'],
      'second' => $row['second'],
      'totalquestion' => $row['totalquestion'],
    'totalmarks' => $row['totalmarks']
  ]);
  }
}
exit(json_encode($response));
?>
