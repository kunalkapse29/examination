<?php
session_start();
include 'connection.php';

if(isset($_SESSION['userid'])){
	$user_id = $_SESSION['userid'];
	$user_name =  $_SESSION['username'];
 }
 if(isset($user_id))
{
?>
<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <title>College HRM</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" >

    <!-- The styles -->
    <link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

    <link href="css/charisma-app.css" rel="stylesheet">
    <link href='bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
    <link href='bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
    <link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
    <link href='bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
    <link href='bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
    <link href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
    <link href='css/jquery.noty.css' rel='stylesheet'>
    <link href='css/noty_theme_default.css' rel='stylesheet'>
    <link href='css/elfinder.min.css' rel='stylesheet'>
    <link href='css/elfinder.theme.css' rel='stylesheet'>
    <link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
    <link href='css/uploadify.css' rel='stylesheet'>
    <link href='css/animate.min.css' rel='stylesheet'>

    <!-- jQuery -->
    <script src="bower_components/jquery/jquery.min.js"></script>
    <link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
    <!-- topbar starts -->
    <div class="navbar navbar-default" role="navigation">

        <div class="navbar-inner">
            <button type="button" class="navbar-toggle pull-left animated flip">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- user dropdown starts -->
            <div class="btn-group pull-right">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs">Logout</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#">Profile</a></li>
                    <li class="divider"></li>
                    <li><a href="logout.php">Logout</a></li>
                </ul>
            </div>
            <!-- user dropdown ends -->

            <!-- theme selector starts -->
            <div class="btn-group pull-right theme-container animated tada">
                <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-tint"></i><span
                        class="hidden-sm hidden-xs"> Change Theme / Skin</span>
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" id="themes">
                    <li><a data-value="classic" href="#"><i class="whitespace"></i> Classic</a></li>
                    <li><a data-value="cerulean" href="#"><i class="whitespace"></i> Cerulean</a></li>
                    <li><a data-value="cyborg" href="#"><i class="whitespace"></i> Cyborg</a></li>
                    <li><a data-value="simplex" href="#"><i class="whitespace"></i> Simplex</a></li>
                    <li><a data-value="darkly" href="#"><i class="whitespace"></i> Darkly</a></li>
                    <li><a data-value="lumen" href="#"><i class="whitespace"></i> Lumen</a></li>
                    <li><a data-value="slate" href="#"><i class="whitespace"></i> Slate</a></li>
                    <li><a data-value="spacelab" href="#"><i class="whitespace"></i> Spacelab</a></li>
                    <li><a data-value="united" href="#"><i class="whitespace"></i> United</a></li>
                </ul>
            </div>
            <!-- theme selector ends -->



        </div>
    </div>
    <!-- topbar ends -->
<div class="ch-container">
    <div class="row">

        <!-- left menu starts -->
        <?php include "studentheader.php"; ?>

        <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Student Dashboard</a>
        </li>
    </ul>
</div>
<div class=" row">
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="Live Event" class="well top-block" href="#">
            <i class="glyphicon glyphicon-user blue"></i>

            <div>Total Live Event</div>
            <div><span id="totalliveevent"></span></div>
            <!-- <span class="notification">6</span> -->
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="Upcomming Event" class="well top-block" href="#">
            <i class="glyphicon glyphicon-star green"></i>

            <div>Total Upcomming Event</div>
            <div><span id="totalupcommingevent"></span></div>
            <!-- <span class="notification green">4</span> -->
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="Total Notice" class="well top-block" href="#">
            <i class="glyphicon glyphicon glyphicon-comment pink"></i>

            <div>Total Notice</div>
            <div><span id="totalnotice"></span></div>
            <!-- <span class="notification yellow">$34</span> -->
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" title="Special Notice" class="well top-block" href="#">
            <i class="glyphicon glyphicon-envelope yellow"></i>

            <div>Special Notice</div>
            <div><span id="totalspecialnotice"></span></div>
            <!-- <span class="notification red">12</span> -->
        </a>
    </div>
</div>

<div class="row">
    <div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i> College Profile</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content row">
                <div class="col-md-12" id="showprofile">


                </div>

            </div>
        </div>
    </div>
		<div class="box col-md-6">
        <div class="box-inner">
            <div class="box-header well">
                <h2><i class="glyphicon glyphicon-info-sign"></i>Student Profile</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <!-- <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a> -->
                </div>
            </div>
            <div class="box-content row">
                <div class="col-md-12" id="studentprofile">


                </div>

            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="box col-md-4">
			<div class="box-inner">
					<div class="box-header well" data-original-title="">
							<h2><i class="glyphicon glyphicon-list"></i> Live Events</h2>

							<div class="box-icon">
									<a href="#" class="btn btn-setting btn-round btn-default"><i
													class="glyphicon glyphicon-cog"></i></a>
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
													class="glyphicon glyphicon-chevron-up"></i></a>
									<a href="#" class="btn btn-close btn-round btn-default"><i
													class="glyphicon glyphicon-remove"></i></a>
							</div>
					</div>
					<div class="box-content" id="liveevents">



					</div>
			</div>
	</div>
    <!--/span-->
    <div class="box col-md-4">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i> Upcomming Events</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content" id="upcommingevents">


            </div>
        </div>
    </div>
    <!--/span-->

    <div class="box col-md-4">
        <div class="box-inner">
            <div class="box-header well" data-original-title="">
                <h2><i class="glyphicon glyphicon-list"></i> Weekly Stat</h2>

                <div class="box-icon">
                    <a href="#" class="btn btn-setting btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    <a href="#" class="btn btn-close btn-round btn-default"><i
                            class="glyphicon glyphicon-remove"></i></a>
                </div>
            </div>
            <div class="box-content">
                <ul class="dashboard-list">
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-arrow-up"></i>
                            <span class="green">92</span>
                            New Comments
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-arrow-down"></i>
                            <span class="red">15</span>
                            New Registrations
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-minus"></i>
                            <span class="blue">36</span>
                            New Articles
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-comment"></i>
                            <span class="yellow">45</span>
                            User reviews
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-arrow-up"></i>
                            <span class="green">112</span>
                            New Comments
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-arrow-down"></i>
                            <span class="red">31</span>
                            New Registrations
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-minus"></i>
                            <span class="blue">93</span>
                            New Articles
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="glyphicon glyphicon-comment"></i>
                            <span class="yellow">254</span>
                            User reviews
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <!--/span-->
</div><!--/row-->
<div class="row">
<div class="box col-md-4">
		<div class="box-inner">
				<div class="box-header well" data-original-title="">
						<h2><i class="glyphicon glyphicon-list"></i> Notice</h2>

						<div class="box-icon">
								<a href="#" class="btn btn-setting btn-round btn-default"><i
												class="glyphicon glyphicon-cog"></i></a>
								<a href="#" class="btn btn-minimize btn-round btn-default"><i
												class="glyphicon glyphicon-chevron-up"></i></a>
								<a href="#" class="btn btn-close btn-round btn-default"><i
												class="glyphicon glyphicon-remove"></i></a>
						</div>
				</div>
				<div class="box-content" id="normalnotice">



				</div>
		</div>
</div>
	<!--/span-->
	<div class="box col-md-4">
			<div class="box-inner">
					<div class="box-header well" data-original-title="">
							<h2><i class="glyphicon glyphicon-list"></i> Special Notice</h2>

							<div class="box-icon">
									<a href="#" class="btn btn-setting btn-round btn-default"><i
													class="glyphicon glyphicon-cog"></i></a>
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
													class="glyphicon glyphicon-chevron-up"></i></a>
									<a href="#" class="btn btn-close btn-round btn-default"><i
													class="glyphicon glyphicon-remove"></i></a>
							</div>
					</div>
					<div class="box-content" id="specialnotice">


					</div>
			</div>
	</div>
	<!--/span-->
</div>



    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->


    <hr>


		<footer class="row">
			<div class="text-center">
				<small>Powered by </small><a href="http://www.xxovek.com/">Xxovek</a>
			</div>
		</footer>


</div><!--/.fluid-container-->

<!-- external javascript -->

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- library for cookie management -->
<script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
<script>
function showprofile(){
	$.ajax({
			 url: "getshowprofile.php",
			 type: "GET",
			 success: function(data) {

						response = JSON.parse(data);
						$("#showprofile").html(response['txteditor']);
					},
					 error: function(data, errorThrown) {
					 }
			 });
}
function showstudentprofile(){
	var html ='';
	$.ajax({
			 url: "getshowstudentprofile.php",
			 type: "GET",
			 success: function(data) {
            // alert(data);
						response = JSON.parse(data);
            html +='<div class="row"><div class="col-sm-6"><b>First Name:</b> '+response['firstname']+'</div><div class="col-sm-6"><b>Last Name:</b>'+response['lastname']+'</div></div>';
						html +='<div class="row"><div class="col-sm-6"><b>Gender:</b>'+response['gender']+'</div><div class="col-sm-6"><b>Mobile No:</b>'+response['mobileno']+'</div></div>';
						html +='<div class="row"><div class="col-sm-6"><b>Email Address:</b>'+response['email']+'</div><div class="col-sm-6"><b>Date-of-Birth:</b>'+response['dateofbirth']+'</div></div>';
					  $("#studentprofile").html(html);
					},
					 error: function(data, errorThrown) {
					 }
			 });
}
function liveevents(){
	$("#liveevents").empty();
	var html ='';
  $.ajax({
 		 url: "getliveevent.php",
 		 type: "GET",
 		 success: function(data) {
 			 var response = JSON.parse(data);
 			 var count = Object.keys(response).length;
			 $("#totalliveevent").html(count);
       html +='<marquee behavior="scroll" direction="up" scrollamount="3">';
 			 for(var i=0;i<count;i++){
 				 html +='<div class="row"><div class="col-sm-12" style="color:skyblue;">'+(i+1)+'.'+response[i]['examtitle']+'';
 				 html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" style="color:skyblue;">'+response[i]['examdesc']+'';
					html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" style="color:orange;"> Exam Starts From '+response[i]['startdate']+'till'+response[i]['enddate']+'<br/>Duration '+response[i]['hour']+'hr'+response[i]['minute']+'min'+response[i]['second']+'sec';
 				 html +='</div></div></br>';
 			 }
			 if(count===0){
			 html +='<div class="row"><div class="col-sm-12" ><font color="red"> <b>No Upcomming Events Available';
			 html +='</font></div></div><br/>';
			}
			 html +='</marquee>';

 			 $("#liveevents").append(html);
 		 }
 	 });
}
function upcommingevents(){
	$("#upcommingevents").empty();
	var html ='';
  $.ajax({
 		 url: "getupcommingevents.php",
 		 type: "GET",
 		 success: function(data) {
 			 var response = JSON.parse(data);
 			 var count = Object.keys(response).length;
			 $("#totalupcommingevent").html(count);
       html +='<marquee behavior="scroll" direction="up" scrollamount="3">';
 			 for(var i=0;i<count;i++){
 				 html +='<div class="row"><div class="col-sm-12" >'+(i+1)+'.'+response[i]['examtitle']+'';
 				 html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" >'+response[i]['examdesc']+'';
					html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" > Exam Starts From '+response[i]['startdate']+'till'+response[i]['enddate']+'<br/>Duration '+response[i]['hour']+'hr'+response[i]['minute']+'min'+response[i]['second']+'sec';
 				 html +='</div></div></br>';
 			 }
			 if(count===0){
				 html +='<div class="row"><div class="col-sm-12" ><font color="red"> <b>No Upcomming Events Available';
				 html +='</font></div></div><br/>';

			 }
			 html +='</marquee>';

 			 $("#upcommingevents").append(html);
 		 }
 	 });
}
function getstudentnotice(){
	$("#normalnotice").empty();
	var html ='';
	$.ajax({
		 url: "getstudentnotice.php",
		 type: "GET",
		 success: function(data) {

			 var response = JSON.parse(data);
			 var count = Object.keys(response).length;
			 $("#totalnotice").html(count);
			 html +='<marquee behavior="scroll" direction="up" scrollamount="3">';
			 for(var i=0;i<count;i++){
				 html +='<div class="row"><div class="col-sm-12" style="color:skyblue;">'+(i+1)+'.';
				 html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" style="color:skyblue;">'+response[i]['txteditor']+'';
					html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" style="color:orange;"> Starts Date '+response[i]['startdate']+'End Date'+response[i]['enddate']+'';
				 html +='</div></div></br>';
			 }
			 if(count===0){
				html +='<div class="row"><div class="col-sm-12" ><font color="red"> <b>No Upcomming Student Notice Available';
				html +='</font></div></div><br/>';

			}
			 html +='</marquee>';

			 $("#normalnotice").append(html);
		 }
	 });
}
function getspecialnotice(){
	$("#specialnotice").empty();
	var html ='';
	$.ajax({
		 url: "getstudentspecialnotice.php",
		 type: "GET",
		 success: function(data) {

			 var response = JSON.parse(data);
			 var count = Object.keys(response).length;
			  $("#totalspecialnotice").html(count);
			 html +='<marquee behavior="scroll" direction="up" scrollamount="3">';
			 for(var i=0;i<count;i++){
				 html +='<div class="row"><div class="col-sm-12" style="color:skyblue;">'+(i+1)+'.';
				 html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" style="color:skyblue;">'+response[i]['txteditor']+'';
					html +='</div></div>';
				 html +='<div class="row"><div class="col-sm-12" style="color:orange;"> Starts Date '+response[i]['startdate']+'End Date'+response[i]['enddate']+'';
				 html +='</div></div></br>';
			 }
			 if(count===0){
				html +='<div class="row"><div class="col-sm-12" ><font color="red"> <b>No Upcomming Student Notice Available';
				html +='</font></div></div><br/>';

			}
			 html +='</marquee>';

			 $("#specialnotice").append(html);
		 }
	 });
}
jQuery(document).ready(function($) {
	  showprofile();
    liveevents();
		upcommingevents();
		showstudentprofile();
		getstudentnotice();
		getspecialnotice();
});
</script>

</body>
</html>
<?php
}
else {
	header("Location:./");
}
?>
